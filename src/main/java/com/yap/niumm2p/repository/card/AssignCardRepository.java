package com.yap.niumm2p.repository.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.card.AssignCard;
@Repository
public interface AssignCardRepository extends JpaRepository<AssignCard, Integer> {

}

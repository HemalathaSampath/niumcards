
package com.yap.niumm2p.repository.card;

import com.yap.niumm2p.model.card.PinBlockTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PinBlockRepository extends JpaRepository<PinBlockTable, Integer> {

}

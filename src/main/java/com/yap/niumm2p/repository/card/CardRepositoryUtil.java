package com.yap.niumm2p.repository.card;

import java.util.UUID;

import com.yap.niumm2p.model.card.*;
import com.yap.niumm2p.pojo.card.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class CardRepositoryUtil {
	@Autowired
	private CardRepository cardRepository;

	@Autowired
	private AssignCardRepository assignCardRepository;

	@Autowired
	private ChannelRestrictionRepository channelRestrictionRepository;

	@Autowired
	CardBlockRepository cardBlockRepository;

	@Autowired
	ActivateCardRepository activateCardRepository;
	
	@Autowired
	PinBlockRepository pinBlockRepository;

	@Autowired
	IssueReplacementCardRepository issueReplacementCardRepository;

	@Autowired
	UnmaskedcardNumberRepository unmaskedcardNumberRepository;

	public void saveCard(CardPojo card, CardResponse cardResponse) {
		Card card1 = new Card();
		BeanUtils.copyProperties(card,card1);
		card1.setResponse(cardResponse);
		cardRepository.save(card1);
	}

	public void saveActivateCard(UUID customerHashId, UUID walletHashId, UUID cardHashId,
			ResponseEntity<ActivateCard> cardRepositoryResponse) {
		cardRepositoryResponse.getBody().setCardHashId(cardHashId.toString());
		cardRepositoryResponse.getBody().setCustomerHashId(customerHashId.toString());
		cardRepositoryResponse.getBody().setWalletHashId(walletHashId.toString());
		activateCardRepository.save(cardRepositoryResponse.getBody());
	}

	public void savePinUpdate(PinBlockPojo pinBlock, UUID customerHashId, UUID walletHashId, UUID cardHashId,
			ResponseEntity<PinBlockResponse> cardRepositoryResponse) {
		PinBlockTable pinBlock1 = new PinBlockTable();
		BeanUtils.copyProperties(pinBlock1, pinBlock);
		pinBlock1.setCardHashId(cardHashId.toString());
		pinBlock1.setCustomerHashId(customerHashId.toString());
		pinBlock1.setWalletHashId(walletHashId.toString());
		pinBlock1.setResponse(cardRepositoryResponse.getBody());
		pinBlockRepository.save(pinBlock1);
	}

	public void saveIssueReplacementCard(IssueReplacementCardPojo issueReplacementCard, UUID customerHashId,
										 UUID walletHashId, ResponseEntity<CardResponse> cardRepositoryResponse) {

		IssueReplacementCard issueReplacementCard1 = new IssueReplacementCard();
		BeanUtils.copyProperties(issueReplacementCard, issueReplacementCard1);
		issueReplacementCard1.setCustomerHashId(customerHashId.toString());
		issueReplacementCard1.setWalletHashId(walletHashId.toString());
		issueReplacementCard1.setResponse(cardRepositoryResponse.getBody());
		issueReplacementCardRepository.save(issueReplacementCard1);
	}

	public void saveBlockORUnblockCard(CardBlockPojo cardblockPojo, UUID customerHashId, UUID walletHashId, UUID cardHashId,
									   ResponseEntity<CardBlockResponse> cardRepositoryResponse) {
		CardBlock cardBlock = new CardBlock();
		BeanUtils.copyProperties(cardblockPojo, cardBlock);
		cardBlock.setCardHashId(cardHashId.toString());
		cardBlock.setCustomerHashId(customerHashId.toString());
		cardBlock.setWalletHashId(walletHashId.toString());
		cardBlock.setResponse(cardRepositoryResponse.getBody());
		cardBlockRepository.save(cardBlock);
	}

	public void saveAssignCard(AssignCardPojo assignCardPojo, UUID customerHashId, UUID walletHashId,
							   ResponseEntity<CardResponse> cardRepositoryResponse) {
		AssignCard assignCard = new AssignCard();
		assignCard.setCustomerHashId(customerHashId.toString());
		assignCard.setWalletHashId(walletHashId.toString());
		BeanUtils.copyProperties(assignCard, assignCardPojo);
		assignCard.setResponse(cardRepositoryResponse.getBody());
		assignCardRepository.save(assignCard);
	}

	public void saveChannelRestriction(ChannelRestrictionPojo channelRestriction, UUID customerHashId, UUID walletHashId,
			UUID cardHashId, ResponseEntity<ChannelRestrictionResponse> cardRepositoryResponse) {
		ChannelRestriction channelRestriction1 = new ChannelRestriction();
		BeanUtils.copyProperties(channelRestriction1,channelRestriction);
		channelRestriction1.setCardHashId(cardHashId.toString());
		channelRestriction1.setCustomerHashId(customerHashId.toString());
		channelRestriction1.setWalletHashId(walletHashId.toString());
		channelRestriction1.setResponse(cardRepositoryResponse.getBody());
		channelRestrictionRepository.save(channelRestriction1);
	}
}

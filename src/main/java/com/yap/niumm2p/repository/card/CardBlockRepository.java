package com.yap.niumm2p.repository.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.card.CardBlock;
@Repository
public interface CardBlockRepository
		extends RevisionRepository<CardBlock, Integer, Integer>, JpaRepository<CardBlock, Integer> {

}

package com.yap.niumm2p.repository.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.card.ActivateCard;

@Repository
public interface ActivateCardRepository
		extends RevisionRepository<ActivateCard, Integer, Integer>, JpaRepository<ActivateCard, Integer> {

}

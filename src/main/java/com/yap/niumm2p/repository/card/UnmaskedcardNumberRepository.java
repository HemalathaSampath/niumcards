package com.yap.niumm2p.repository.card;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yap.niumm2p.model.card.UnmaskedcardNumber;
import org.springframework.stereotype.Repository;

@Repository
public interface UnmaskedcardNumberRepository extends JpaRepository<UnmaskedcardNumber, Integer>{

}

package com.yap.niumm2p.repository.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.card.IssueReplacementCard;

@Repository
public interface IssueReplacementCardRepository
		extends RevisionRepository<IssueReplacementCard, Integer, Integer>, JpaRepository<IssueReplacementCard, Integer> {

}

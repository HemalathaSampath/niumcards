package com.yap.niumm2p.repository.card;

import com.yap.niumm2p.model.card.CvvResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CvvResponseRepository extends JpaRepository<CvvResponse, Integer> {
}

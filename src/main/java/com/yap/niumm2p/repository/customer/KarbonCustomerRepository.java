package com.yap.niumm2p.repository.customer;

import com.yap.niumm2p.model.customer.KarbonCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KarbonCustomerRepository extends JpaRepository<KarbonCustomer, String> {

}

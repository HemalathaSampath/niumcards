package com.yap.niumm2p.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;
import com.yap.niumm2p.model.customer.RFIDetails;

@Repository
public interface RFIDetailsrepository extends RevisionRepository<RFIDetails,Long, Integer>, JpaRepository<RFIDetails, Long> {
	
}

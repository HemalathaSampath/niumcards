package com.yap.niumm2p.repository.customer;

import com.yap.niumm2p.model.customer.TermsAndCondition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TermsAnsConditionRepository extends JpaRepository<TermsAndCondition, Integer> {
}

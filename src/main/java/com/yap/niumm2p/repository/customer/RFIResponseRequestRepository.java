package com.yap.niumm2p.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;
import com.yap.niumm2p.model.customer.RFIResponseRequest;

@Repository
public interface RFIResponseRequestRepository
		extends RevisionRepository<RFIResponseRequest, Integer, Integer>, JpaRepository<RFIResponseRequest, Integer> {

}

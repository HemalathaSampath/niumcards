package com.yap.niumm2p.repository.customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.model.customer.*;
import com.yap.niumm2p.pojo.customer.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import java.io.IOException;
import java.util.*;

@Slf4j
@Repository
public class CustomerRepositoryUtil {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMinRepository customerMinRepository;

    @Autowired
    private RFIDetailsrepository rFIDetailsrepository;

    @Autowired
    private GetCustomerRepository getCustomerRepository;

    @Autowired
    private CustomerIdentDocumentRepository customerIdentDocumentRepository;

    @Autowired
    private CustomerIdentificationDocRepository customerIdentificationDocRepository;

    @Autowired
    private RFIResponseRequestRepository rFIResponseRequestRepository;

    @Autowired
    private KarbonCustomerRepository karbonCustomerRepository;

    @Autowired
    private TermsAnsConditionRepository termsAnsConditionRepository;

    @Autowired
    private ObjectMapper mapper;

    public void saveCustomer(CustomerPojo customerPojo, KarbonCustomerPojo karbonCustomerPojo, ResponseEntity<CustomerResponsePojo> result) throws IOException {
        Customer customer = new Customer();
        Optional<CustomerResponsePojo> customerResponsePojo = Optional.ofNullable(result.getBody());
        if (customerResponsePojo.isPresent()) {
            customer.setCustomerHashId(customerResponsePojo.get().getCustomerHashId());
            customer.setWalletHashId(customerResponsePojo.get().getWalletHashId());
        }
        BeanUtils.copyProperties(customerPojo, customer);
        customerRepository.save(customer);
        KarbonCustomer karbonCustomer = new KarbonCustomer();
        BeanUtils.copyProperties(karbonCustomerPojo, karbonCustomer);
        karbonCustomer.setCustomerHashId(result.getBody().getCustomerHashId());
        karbonCustomer.setWalletHashId(result.getBody().getWalletHashId());
        karbonCustomerRepository.save(karbonCustomer);
    }

    public void saveMinCustomer(CustomerMinPojo customerMinPojo, ResponseEntity<CustomerMinResponsePojo> result) {
        CustomerMin newCustomerMin = new CustomerMin();
        BeanUtils.copyProperties(customerMinPojo, newCustomerMin);
        Optional<CustomerMinResponsePojo> customerMinResponsePojo = Optional.ofNullable(result.getBody());
        if (customerMinResponsePojo.isPresent()) {
            newCustomerMin.setCustomerHashId(result.getBody().getCustomerHashId());
            newCustomerMin.setWalletHashId(result.getBody().getWalletHashId());
            newCustomerMin.setRedirectUrl(result.getBody().getRedirectUrl());
        }
        customerMinRepository.save(newCustomerMin);
    }

    public void saveUpdateCustomer(UpdateCustomerPojo updateCustomerPojo, String customerHashId) {
        Optional<Customer> customerOptional = customerRepository.findByCustomerHashId(customerHashId);
        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            BeanUtils.copyProperties(updateCustomerPojo, customer);
            customerRepository.save(customer);
        }
    }

    public void saveGetCustomer(ResponseEntity<GetCustomerPojo> response, String customerHashId) {
        GetCustomer customer = new GetCustomer();
        customer.setCustomerHashId(customerHashId);
        Optional<GetCustomerPojo> getCustomerPojo = Optional.ofNullable(response.getBody());
        getCustomerPojo.ifPresent(customerPojo -> BeanUtils.copyProperties(customerPojo, customer));
        Optional<List<RFIDetails>> rfiDetails = Optional.ofNullable(customer.getRfiDetails());
        if (rfiDetails.isPresent()) {
            for (RFIDetails rfiDetail : rfiDetails.get()) {
                RFIDetails rfiDetailObj = new RFIDetails();
                BeanUtils.copyProperties(rfiDetail, rfiDetailObj);
                rfiDetailObj.setCustomerHashId(customerHashId);
                rFIDetailsrepository.save(rfiDetailObj);
            }
        }
        getCustomerRepository.save(customer);
    }

    public void saveCustomerUploadDoc(CustomerUploadDocPojo customerUploadDocPojo, ResponseEntity<Object> response) {

        for (IdentDocPojo identDocPojo : customerUploadDocPojo.getIdentificationDoc()) {
            IdentDoc identDoc = new IdentDoc();
            BeanUtils.copyProperties(identDocPojo, identDoc);
            for (IdentDocumentPojo identDocumentPojo : identDocPojo.getIdentificationDocument()) {
                IdentDocument identdocument = new IdentDocument();
                BeanUtils.copyProperties(identDocumentPojo, identdocument);
                customerIdentDocumentRepository.save(identdocument);
            }
            Optional<Object> customerUploadDocResponse = Optional.ofNullable(response.getBody());
            customerUploadDocResponse.ifPresent(o -> identDocPojo.setStatus(o.toString()));
            customerIdentificationDocRepository.save(identDoc);
        }
    }

    public void saveRFIUploadDocument(RFIDocumentUploadPojo rfiDocumentUploadPojo, ResponseEntity<RFIDocumentUploadResponsePojo> response) {

        List<RFIResponseRequestPojo> rfiResponseRequestPojo = rfiDocumentUploadPojo.getRfiResponseRequest();
        for (RFIResponseRequestPojo rfiRRPojo : rfiResponseRequestPojo) {
            RFIResponseRequest rfiResponseRequest = new RFIResponseRequest();
            BeanUtils.copyProperties(rfiRRPojo, rfiResponseRequest);
            Optional<RFIDocumentUploadResponsePojo> rfiDocumentUploadResponsePojo = Optional.ofNullable(response.getBody());
            if (rfiDocumentUploadResponsePojo.isPresent()) {
                rfiResponseRequest.setComplianceId(rfiDocumentUploadResponsePojo.get().getComplianceId());
                rfiResponseRequest.setStatus(rfiDocumentUploadResponsePojo.get().getStatus());
                rfiResponseRequest.setRfiId(rfiDocumentUploadResponsePojo.get().getRfiId());
            }
            rFIResponseRequestRepository.save(rfiResponseRequest);
        }
    }

    public void saveTermsAndCondition(TermsAndConditionPojo termsAndConditionPojo, ResponseEntity<TermsAndConditionResponsePojo> result) {
        TermsAndCondition termsAndCondition = new TermsAndCondition();
        BeanUtils.copyProperties(termsAndConditionPojo, termsAndCondition);
        Optional<TermsAndConditionResponsePojo> termsAndConditionResponsePojo = Optional.ofNullable(result.getBody());
        termsAndConditionResponsePojo.ifPresent(andConditionResponsePojo -> BeanUtils.copyProperties(andConditionResponsePojo, termsAndCondition));
        termsAnsConditionRepository.save(termsAndCondition);
    }
}

package com.yap.niumm2p.repository.customer;

import com.yap.niumm2p.model.customer.GetCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GetCustomerRepository extends JpaRepository<GetCustomer, Integer> {
}

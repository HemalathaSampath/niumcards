package com.yap.niumm2p.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yap.niumm2p.model.customer.CustomerMin;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerMinRepository extends JpaRepository<CustomerMin, Integer> {

}

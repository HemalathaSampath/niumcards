package com.yap.niumm2p.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;
import com.yap.niumm2p.model.customer.Customer;
import java.util.Optional;

@Repository
public interface CustomerRepository extends RevisionRepository<Customer,Integer, Integer>,  JpaRepository<Customer, Integer> {

	@Query(value = "SELECT c FROM Customer as c WHERE c.customerHashId= ?1")
	 Optional<Customer> findByCustomerHashId(String customerHashId);

}

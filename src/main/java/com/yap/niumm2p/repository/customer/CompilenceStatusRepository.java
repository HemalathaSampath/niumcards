package com.yap.niumm2p.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.yap.niumm2p.model.customer.CompilenceStatus;

@Repository
public interface CompilenceStatusRepository extends JpaRepository<CompilenceStatus, Integer>{

}

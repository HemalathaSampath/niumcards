package com.yap.niumm2p.repository.clientmanagement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.clientmanagement.ClientPrefundRequest;

@Repository
public interface ClientPrefundRequestRepository extends RevisionRepository<ClientPrefundRequest, Integer, Integer>,
        JpaRepository<ClientPrefundRequest, Integer> {
}

package com.yap.niumm2p.repository.clientmanagement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.clientmanagement.FetchPrefundBalance;
@Repository
public interface FetchPrefundBalanceRepository
		extends RevisionRepository<FetchPrefundBalance, Integer, Integer>, JpaRepository<FetchPrefundBalance, Integer> {

}

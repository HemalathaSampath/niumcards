package com.yap.niumm2p.repository.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.wallet.P2PTransfer;
@Repository
public interface P2PTransferRepository extends JpaRepository<P2PTransfer, Integer> {

}

package com.yap.niumm2p.repository.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.wallet.BalanceTransfer;
@Repository
public interface BalanceTransferRepository extends JpaRepository<BalanceTransfer, Integer> {

}

package com.yap.niumm2p.repository.wallet;

import java.util.UUID;
import com.yap.niumm2p.pojo.wallet.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import com.yap.niumm2p.model.wallet.BalanceTransfer;
import com.yap.niumm2p.model.wallet.BalanceTransferResponse;
import com.yap.niumm2p.model.wallet.FundWallet;
import com.yap.niumm2p.model.wallet.FundWalletResponse;
import com.yap.niumm2p.model.wallet.GetWalletTransactions;
import com.yap.niumm2p.model.wallet.P2PTransfer;
import com.yap.niumm2p.model.wallet.P2PTransferResponse;
import com.yap.niumm2p.model.wallet.ReFund;
import com.yap.niumm2p.model.wallet.ReFundResponse;

@Repository
public class WalletRepositoryUtil {

	@Autowired
	private BalanceTransferRepository balanceTransferRepository;
	@Autowired
	private FundWalletRepository fundWalletRepository;
	@Autowired
	private GetExchangeRateRepository getExchangeRateRepository;
	@Autowired
	private GetWalletBalanceRepository getWalletBalanceRepository;
	@Autowired
	private GetWalletTransactionsRepository getWalletTransactionsRepository;
	@Autowired
	private P2PTransferRepository p2PTransferRepository;
	@Autowired
	private ReFundRepository reFundRepository;

	public void saveBalanceTransfer(BalanceTransferPojo balanceTransfer, UUID customerHashId, UUID walletHashId,
									ResponseEntity<BalanceTransferResponsePojo> responsePojo) {
		BalanceTransfer balTrans = new BalanceTransfer();
		balTrans.setCustomerHashId(customerHashId.toString());
		balTrans.setWalletHashId(walletHashId.toString());
		BeanUtils.copyProperties(balTrans,balanceTransfer);
		BalanceTransferResponse response = new BalanceTransferResponse();
		BeanUtils.copyProperties(response, responsePojo);
		balTrans.setResponse(response);
		balanceTransferRepository.save(balTrans);
	}
	
	public void saveP2PTransfer(P2PTransferPojo p2PTransferPojo, UUID customerHashId, UUID walletHashId,
								ResponseEntity<P2PTransferResponse> response) {
		P2PTransfer p2PTransfer = new P2PTransfer();
		BeanUtils.copyProperties(p2PTransferPojo, p2PTransfer);
		p2PTransfer.setResponse(response.getBody());
		p2PTransfer.setCustomerHashId(customerHashId.toString());
		p2PTransfer.setWalletHashId(walletHashId.toString());
		p2PTransferRepository.save(p2PTransfer);
	}
	public void saveFundWallet(FundWalletPojo fundWalletPojo, UUID customerHashId, UUID walletHashId,
							   ResponseEntity<FundWalletResponsePojo> response) {
		FundWallet fundWallet =  new FundWallet();
		BeanUtils.copyProperties(fundWallet, fundWalletPojo);
		FundWalletResponse fundWalletResponse = new FundWalletResponse();
		BeanUtils.copyProperties(fundWalletResponse,response.getBody());
		fundWallet.setCustomerHashId(customerHashId.toString());
		fundWallet.setWalletHashId(walletHashId.toString());
		fundWalletRepository.save(fundWallet);
	}
	public void saveWithdrawFromWallet(ReFundPojo reFundPojo, UUID customerHashId, UUID walletHashId,
			ResponseEntity<ReFundResponse> response) {
		ReFund reFund = new ReFund();
		BeanUtils.copyProperties(reFundPojo, reFund);
		reFund.setResponse(response.getBody());
		reFund.setCustomerHashId(customerHashId.toString());
		reFund.setWalletHashId(walletHashId.toString());
		reFundRepository.save(reFund);
	}
	public void saveWalletTransaction( UUID customerHashId, UUID walletHashId,
			ResponseEntity<GetWalletTransactions> transactionResponse) {
		transactionResponse.getBody().setCustomerHashId(customerHashId.toString());
		transactionResponse.getBody().setWalletHashId(walletHashId.toString());
		getWalletTransactionsRepository.save(transactionResponse.getBody());
	}

}

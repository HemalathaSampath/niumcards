package com.yap.niumm2p.repository.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.wallet.FundWallet;
@Repository
public interface FundWalletRepository extends JpaRepository<FundWallet, Integer> {

}

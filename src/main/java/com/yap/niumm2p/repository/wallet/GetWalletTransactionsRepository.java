package com.yap.niumm2p.repository.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.wallet.GetWalletTransactions;
@Repository
public interface GetWalletTransactionsRepository extends JpaRepository<GetWalletTransactions, Integer> {

}

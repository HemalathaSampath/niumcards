package com.yap.niumm2p.repository.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yap.niumm2p.model.wallet.ReFund;

@Repository
public interface ReFundRepository extends JpaRepository<ReFund, Integer> {

}

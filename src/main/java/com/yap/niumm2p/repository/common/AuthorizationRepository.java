package com.yap.niumm2p.repository.common;

import com.yap.niumm2p.model.transaction.Authorization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends JpaRepository<Authorization, Integer>, RevisionRepository<Authorization, Integer, Integer> {
}

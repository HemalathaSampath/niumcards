package com.yap.niumm2p.repository.common;

import com.yap.niumm2p.model.webhooks.WebHook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebHookRepository extends JpaRepository<WebHook,Integer>, RevisionRepository<WebHook, Integer, Integer> {
}

package com.yap.niumm2p.repository.common;

import com.yap.niumm2p.model.transaction.Kit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KitRepository extends JpaRepository<Kit, Integer>, RevisionRepository<Kit, Integer, Integer> {

   @Query(value = "SELECT k FROM Kit k WHERE k.card_no = ?1")
   Optional<Kit> findbycard_no(Integer cardNo);
}

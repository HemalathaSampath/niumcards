package com.yap.niumm2p.repository.common;

import com.yap.niumm2p.model.transaction.AuthorizationKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorizationKeyRepository extends JpaRepository<AuthorizationKey, Integer> {
}

package com.yap.niumm2p.repository.common;

import com.yap.niumm2p.model.transaction.TransactionFees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionFeesRepository extends JpaRepository<TransactionFees, Integer>, RevisionRepository<TransactionFees, Integer, Integer> {
}

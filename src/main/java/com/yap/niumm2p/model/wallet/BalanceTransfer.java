package com.yap.niumm2p.model.wallet;


import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="BalanceTransfer")
public class BalanceTransfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer balanceTransferId;
	private String amount;
	private String destinationCurrency;
	private String sourceCurrency;
	@JsonIgnore
	@Embedded
	private BalanceTransferResponse response;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
}

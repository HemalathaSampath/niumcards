package com.yap.niumm2p.model.wallet;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class P2PTransferResponse {

	private String status;
	private String retrievalReferenceNumber;
}

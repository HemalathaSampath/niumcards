package com.yap.niumm2p.model.wallet;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class PaymentMethods {
	
	private String bankName;
	private String accountNumber;
	private String accountName;
	private String qrCode;
	private String uen;
	private String status;
	
}

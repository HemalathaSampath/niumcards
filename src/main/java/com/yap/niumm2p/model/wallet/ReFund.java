package com.yap.niumm2p.model.wallet;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="ReFund")
public class ReFund {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer reFundId;
	private Double amount;
	private String comments;
	private String currencyCode;
	private String pocketName;
	private String refundMode;
	@JsonIgnore
	@Embedded
	private ReFundResponse response;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
}

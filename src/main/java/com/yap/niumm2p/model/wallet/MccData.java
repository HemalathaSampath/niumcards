package com.yap.niumm2p.model.wallet;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class MccData {
	
	private String mccCode;
	private String description;
	private String merchantCategory;

}

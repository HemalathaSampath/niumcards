package com.yap.niumm2p.model.wallet;

import java.util.UUID;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class Content {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String acquirerCountryCode;
	private String acquiringInstitutionCode;
	private String authCurrencyCode;
	private UUID cardHashId;
	private Double cardTransactionAmount;
	private String createdAt;
	private String billingCurrencyCode;
	private String previousBalance;
	private String currentWithHoldingBalance;
	private String dateOfTransaction;
	private Boolean debit;
	private Double effectiveAuthAmount;
	private String maskedCardNumber;
	private String mcc;
	private String merchantID;
	private String merchantName;
	private String merchantCity;
	private String merchantCountry;
	private String posConditionCode;
	private String posEntryCapabilityCode;
	private String processingCode;
	private Double billingReplacementAmount;
	private Double transactionReplacementAmount;
	private String retrievalReferenceNumber;
	private Double settlementAmount;
	private String settlementStatus;
	private String status;
	private String systemTraceAuditNumber;
	private String terminalID;
	private String authCode;
	private String transactionCurrencyCode;
	private String transactionType;
	private String updatedAt;
	private String rhaTransactionId;
	private String partnerReferenceNumber;
	
}

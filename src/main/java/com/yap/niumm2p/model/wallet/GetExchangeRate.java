package com.yap.niumm2p.model.wallet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="GetExchangeRate")
public class GetExchangeRate {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer getExchangeRateId;
	private Double exchangeRate;
	private String lastUpdated;
	private String sourceCurrencyCode;
	private String destinationCurrencyCode;
	private Double markupRate;
	private Double markupAmount;
	private Double sourceAmount;
	private Double destinationAmount;
	private Boolean hasBalance;
}

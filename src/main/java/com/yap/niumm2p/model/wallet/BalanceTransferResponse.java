package com.yap.niumm2p.model.wallet;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class BalanceTransferResponse {
	
	private String fxRate;
	@Column(name = "responseSourceCurrency")
	private String sourceCurrency;
	@Column(name = "responseDestinationCurrency")
	private String destinationCurrency;
	private Double markupAmount;
	private Double markupAmountPercentage;
	private Double sourceAmount;
	private Double destinationAmount;
}

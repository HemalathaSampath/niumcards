package com.yap.niumm2p.model.wallet;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ReFundResponse {
	
	private String message;
	private String systemReferenceNumber;

}

package com.yap.niumm2p.model.wallet;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="P2PTransfer")
public class P2PTransfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer p2PTransferId;
	private String destinationWalletHashId;
	private Double amount;
	private String currencyCode;
	@JsonIgnore
	@Embedded
	private P2PTransferResponse response;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
}

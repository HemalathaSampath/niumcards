package com.yap.niumm2p.model.wallet;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class FundWalletResponse {

	private String systemReferenceNumber;
	private Double destinationAmount;
	@Column(name = "responseDestinationCurrencyCode")
	private String destinationCurrencyCode;
	private Double sourceAmount;
	@Column(name = "responseSourceCurrencyCode")
	private String sourceCurrencyCode;
	@Embedded
	private List<PaymentMethods> paymentMethods;

}

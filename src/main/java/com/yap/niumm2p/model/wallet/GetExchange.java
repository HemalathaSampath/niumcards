package com.yap.niumm2p.model.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetExchange {
	
	private Double amount;
	private String destinationCurrencyCode;
	private String sourceCurrencyCode;

}

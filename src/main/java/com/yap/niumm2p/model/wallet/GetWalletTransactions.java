package com.yap.niumm2p.model.wallet;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="GetWalletTransactions")
public class GetWalletTransactions {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long id;
	private String totalPages;
	@OneToMany
	private List<Content> content;
	private String totalElements;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;

}

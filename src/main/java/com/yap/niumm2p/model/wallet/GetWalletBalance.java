package com.yap.niumm2p.model.wallet;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="GetWalletBalance")
public class GetWalletBalance {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer getWalletBalanceId;
	private String curSymbol;
	private Double balance;
	private Double withHoldingBalance;
	@Embedded
	private List<MccData> mccData;
	private String pocketName;
	@JsonProperty("default")
	private Boolean defaultt;
	private String isoCode;
	
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
	
}

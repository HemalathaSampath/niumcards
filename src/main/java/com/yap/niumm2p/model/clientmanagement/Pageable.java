package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Pageable {	
		@Embedded
		private Sort sort;
		private String pageSize;
		private String pageNumber;
		private String offset;
		private Boolean paged;
		private Boolean unpaged;
		private Boolean last;
		private String totalElements;
		private String totalPages;
		private String numberOfElements;
		private Boolean first;
		private String size;
		private String number;
		private Boolean empty;
		

}

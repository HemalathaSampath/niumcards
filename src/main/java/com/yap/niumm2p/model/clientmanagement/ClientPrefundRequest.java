package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ClientPrefundRequest")
@Entity
@Audited
public class ClientPrefundRequest {	
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@JsonIgnore
		private Integer clientPrefundRequestId;			
		private String bankReferenceNumber;
		private String clientAccountNumber;
		private String comments;
		private String currencyCode;
		private String dateOfTransfer;
		private String niumAccountNumber;
		private String amount;
		private String requesterId;
		@JsonIgnore
		@Embedded
		private ClientPrefundRequestResponse response;
		
}

package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PrefundContent")
@Audited
public class PrefundContent {	
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@JsonIgnore
		private Integer prefundContentId;
		@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
		@JoinColumn(name = "fetchPrefundRequestId")
		private FetchPrefundRequest fetchPrefundRequest;
		@Column(name = "contentId")
		private String id;
		@Column(name = "contentCreatedAt")
		private String createdAt;
		@Column(name = "contentUpdatedAt")
		private String updatedAt;
		private Double amount;
		private String bankReferenceNumber;
		private String imageUrl;
		private String comments;
		private String status;
		private String approverId;
		private String requesterId;
		private String systemReferenceNumber;
		private String dateOfTransfer;
		private String currencyCode;
		private String clientAccountNumber;
		private String niumAccountNumber;
		@Column(name = "ContentClientId")
		private String clientId;
		@Embedded
		private ClientAccounts clientAccounts;
}

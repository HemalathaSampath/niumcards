package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ClientAccounts {	
		
		@Column(name = "clientAccountsId")
		private String id;
		@Column(name = "clientCreatedAt")
		private String createdAt;
		@Column(name = "clientUpdatedAt")
		private String updatedAt;
		private String accountType;
		private String balance;
		@Embedded
		private Currency currency;
		@Column(name = "clientStatus")
		private String status;
			
}

package com.yap.niumm2p.model.clientmanagement;

import java.util.List;
import javax.persistence.*;
import org.hibernate.envers.Audited;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "FetchPrefundRequest")
@Entity
@Audited
public class FetchPrefundRequest {	
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@JsonIgnore
		private  Integer fetchPrefundRequestId;
		@OneToMany(mappedBy = "fetchPrefundRequest", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
		private List<PrefundContent> content;
		@Embedded
		private Pageable pageable;

}

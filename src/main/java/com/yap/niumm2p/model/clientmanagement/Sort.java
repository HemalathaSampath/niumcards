package com.yap.niumm2p.model.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Sort {
    private Boolean sorted;
    private Boolean unsorted;
    @Column(name = "sortempty")
    private Boolean empty;
}

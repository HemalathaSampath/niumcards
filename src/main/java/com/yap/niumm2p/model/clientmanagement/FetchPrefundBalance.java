package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "FetchPrefundBalance")
@Entity
public class FetchPrefundBalance {	
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer fetchPrefundBalanceId;
		private String createdAt;
		private String updatedAt;
		private String accountType;
		private Double balance;
		private Boolean isDefault;

}

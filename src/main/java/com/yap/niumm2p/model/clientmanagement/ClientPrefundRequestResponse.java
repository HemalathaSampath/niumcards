package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ClientPrefundRequestResponse {	
					
		private String message;
		private String status;
		@Column(name = "responseAmount")
		private Double amount;
		private String systemReferenceNumber;

}

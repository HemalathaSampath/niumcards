package com.yap.niumm2p.model.clientmanagement;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Currency {	
		
		@Column(name = "currencyId")
		private String id;
		@Column(name = "currencyCreatedAt")
		private String createdAt;
		@Column(name = "currencyUpdatedAt")
		private String updatedAt;
		private String isoCode;
		private String curSymbol;
		private String localCode;
		private String unit;
		@Column(name = "currencyClientId")
		private String clientId;
		private String authorizationOrder;
		@Column(name = "default")
		@JsonProperty("default")
		private String defaultt;

}

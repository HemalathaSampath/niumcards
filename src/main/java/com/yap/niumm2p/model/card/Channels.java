package com.yap.niumm2p.model.card;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Channels {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer channelsId;
	private String channel;
	private String status;
}

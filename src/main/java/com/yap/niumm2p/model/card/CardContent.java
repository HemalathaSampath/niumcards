package com.yap.niumm2p.model.card;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="CardContent")
public class CardContent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long cardContentId;
	private String cardHashId;
	private String maskedCardNumber;
	private String cardType;
	private String cardStatus;
	private String proxyNumber;
	private String embossingLine1;
	private String embossingLine2;
	private String logoId;
	private String plasticId;
	private String blockReason;
	private String replacedOn;
	private String countryCode;
	private String issuanceMode;
	private String issuanceType;
	private Boolean isNonPerso;
	private String createdAt;
	private String updatedAt;

}

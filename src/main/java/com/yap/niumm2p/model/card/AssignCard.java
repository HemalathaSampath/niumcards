package com.yap.niumm2p.model.card;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="AssignCard")
public class AssignCard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer assignCardId;
	private String accountNumber;
	private String cardNumberLast4Digits;
	
	@JsonIgnore
	@Embedded
	private CardResponse response;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;	
	
}

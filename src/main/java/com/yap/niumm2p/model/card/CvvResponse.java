package com.yap.niumm2p.model.card;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="CvvResponse")
public class CvvResponse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer id;
	private String cvv;
	private String expiry;
	private String cardHashId;
	@JsonIgnore
	private String clientHashId;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
	
}

package com.yap.niumm2p.model.card;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="Card")
@Audited
public class Card {
		 @Id
		 @GeneratedValue(strategy = GenerationType.AUTO)
		 @JsonIgnore
	  private int cardId;
	  private String cardIssuanceAction;
	  private String cardFeeCurrencyCode;
	  private int cardExpiry;
	  private String cardType;
	  private String embossingLine1;
	  private String embossingLine2;
	  private boolean nonPerso;
	  private String issuanceMode;
	  private int logoId;
	  private int plasticId;
	  @JsonIgnore
	  private int customerHashId;
	  @JsonIgnore
	  @Embedded
	  private CardResponse response;
	  
}

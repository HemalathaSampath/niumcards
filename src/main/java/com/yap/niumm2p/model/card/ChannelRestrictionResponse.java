package com.yap.niumm2p.model.card;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ChannelRestrictionResponse {

	private String status;
	private String message;
	private String code;
	private String body;

}

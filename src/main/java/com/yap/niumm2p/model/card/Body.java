package com.yap.niumm2p.model.card;

import java.util.List;
import javax.persistence.Embeddable;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Body {
	@OneToMany
	private List<Channels> channels;
}

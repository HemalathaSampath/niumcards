package com.yap.niumm2p.model.card;

import javax.persistence.Embedded;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetChannelRestrictionResponse {
	private String status;
	private String message;
	private String code;
	@Embedded
	private Body body;
}

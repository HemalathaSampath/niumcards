package com.yap.niumm2p.model.card;

import java.util.UUID;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class CardResponse {

	private UUID cardHashId;
	private String cardActivationStatus;
	private String maskedCardNumber;

}

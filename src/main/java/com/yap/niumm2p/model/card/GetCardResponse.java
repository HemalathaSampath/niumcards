package com.yap.niumm2p.model.card;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="GetCardResponse")
public class GetCardResponse {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long getCardResponseId;	
	private String totalPages;
	@Embedded
	private List<CardContent> content;
	private String totalElements;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String cardHashId;
	@JsonIgnore
	private String walletHashId;
}

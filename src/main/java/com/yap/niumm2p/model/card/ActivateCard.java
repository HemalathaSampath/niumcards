package com.yap.niumm2p.model.card;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ActivateCard")
@Audited
public class ActivateCard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer activateCardId;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String walletHashId;
	@JsonIgnore
	private String cardHashId;
	private String activationStatus;

}

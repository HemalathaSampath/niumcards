package com.yap.niumm2p.model.card;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="ChannelRestriction")
public class ChannelRestriction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long channelRestrictionId;
	private String action;
	private String channel;
	@JsonIgnore
	private String customerHashId;
	@JsonIgnore
	private String cardHashId;
	@JsonIgnore
	private String walletHashId;
	@JsonIgnore
	@Embedded
	private ChannelRestrictionResponse response;
}

package com.yap.niumm2p.model.customer;

import java.util.UUID;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name ="RFIDetails")
@Entity
public class RFIDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long rfiDetailsId;
	private UUID rfiHashId;
	private String description;
	private String documentType;
	private String remarks;
	private String customerHashId;
	@ManyToOne
	@JoinColumn(name="getCustomerId",  nullable=false)
	private GetCustomer getCustomer;
}

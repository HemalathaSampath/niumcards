package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class RFIDocumentUpload {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rfiDocumenUploadId;
    @OneToMany
    private List<RFIResponseRequest> rfiResponseRequest;
    private String complianceId;
    private String status;
    private String rfiId;
}

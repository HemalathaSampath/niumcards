package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class RFIIdentDoc {

    private String identificationType;
    private String identificationValue;
    private String identificationDocExpiry;
    private String identificationDocIssuanceCountry;
    private String identificationDocHolderName;
    private String identificationDocColor;
    private String identificationDocReferenceNumber;
    private String identificationIssuingDate;
    private String identificationIssuingAuthority;
}

package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "SecurityQuestionInfo")
public class SecurityQuestionInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer securityQuestionInfoId;
    private String securityQuestion;
    private String securityAnswer;
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;
}

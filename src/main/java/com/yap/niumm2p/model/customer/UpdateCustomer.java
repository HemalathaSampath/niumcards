package com.yap.niumm2p.model.customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "UpdateCustomer")
@Entity
public class UpdateCustomer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer updateCusatomerId;
	private String billingAddress1;
	private String billingAddress2;
	private String billingCity;
	private String billingLandmark;
	private String billingState;
	private String billingZipCode;
	private String correspondenceAddress1;
	private String correspondenceAddress2;
	private String correspondenceCity;
	private String correspondenceLandmark;
	private String correspondenceState;
	private String correspondenceZipCode;
	private String countryCode;
	private String deliveryAddress1;
	private String deliveryAddress2;
	private String deliveryCity;
	private String deliveryState;
	private String deliveryZipCode;
	private String email;
	private String mobile;
	private String employeeId;
}

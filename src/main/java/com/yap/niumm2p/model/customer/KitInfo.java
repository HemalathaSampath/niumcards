package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "KitInfo")
public class KitInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer kitInfoId;
    private String KitNo;
    private String cardType;
    private String cardCategory;
    private String cardRegStatus;
    private String expDate;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;
}

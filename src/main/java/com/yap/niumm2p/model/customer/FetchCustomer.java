package com.yap.niumm2p.model.customer;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "FetchCustomer")
@Entity
public class FetchCustomer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer fetchCustomerId;
	
	private String createdAt;
	private String updatedAt;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String preferredName;
	private String dateOfBirth;
	private String gender;
	private String designation;
	private String employeeId;
	private String email;
	private String mobile;
	private String country;
	private String kycMode;
	private String nationality;
	private Boolean kycCustomer;
	private String complianceStatus;
	private UUID customerHashId;
	private UUID walletHashId;

}

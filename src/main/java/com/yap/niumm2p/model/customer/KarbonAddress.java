package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "KarbonAddress")
public class KarbonAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer karbonAddressId;
    @ManyToOne
    @JoinColumn(name = "KarbonAddressDTOId")
    private KarbonAddressDTO karbonAddressDTO;
    private String title;
    private String aliasName;
    private String fourthLine;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String country;
    private String pinCode;
}

package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class KarbonAddressDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer KarbonAddressDTOId;
    @OneToOne
    @JoinColumn(name = "entityId")
    private KarbonCustomer karbonCustomer;
    private String contactNo1;
    private String contactNo2;
    private String emailAddress1;
    private String emailAddress2;
    private String notification;
    @OneToMany(mappedBy = "karbonAddressDTO", cascade = CascadeType.ALL)
    private List<KarbonAddress> address;
}

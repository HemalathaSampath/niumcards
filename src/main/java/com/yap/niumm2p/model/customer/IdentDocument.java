package com.yap.niumm2p.model.customer;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class IdentDocument {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String identDocumentId;
	private String rfiHashId;
	private String fileName;
	private String fileType;
	private String document;
	@ManyToOne
	@JoinColumn(name="identDocId", nullable=false)
	private IdentDoc identDoc;
}

package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class GetCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	 private Integer getCustomerId;
	 private String title = null;
	 private String firstName;
	 private String middleName;
	 private String lastName;
	 private String preferredName;
	 private String dateOfBirth;
	 private String gender;
	 private String designation = null;
	 private String employeeId = null;
	 private String nationality;
	 private String email;
	 private String countryCode = null;
	 private String mobile;
	 private String deliveryAddress1;
	 private String deliveryAddress2;
	 private String deliveryCity;
	 private String deliveryLandmark;
	 private String deliveryState;
	 private String deliveryZipCode;
	 private String billingAddress1;
	 private String billingAddress2;
	 private String billingCity;
	 private String billingLandmark;
	 private String billingState;
	 private String billingZipCode;
	 private String correspondenceAddress1;
	 private String correspondenceAddress2;
	 private String correspondenceCity;
	 private String correspondenceLandmark;
	 private String correspondenceState;
	 private String correspondenceZipCode;
	 private String complianceStatus;
	 private String customerHashId;
	 @OneToMany(mappedBy = "getCustomer")
	 private List<RFIDetails> rfiDetails;
}

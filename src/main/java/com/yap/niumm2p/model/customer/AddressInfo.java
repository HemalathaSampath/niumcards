package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "AddressInfo")
public class AddressInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer addressInfoId;
    @ManyToOne
    @JoinColumn(name = "communicationId")
    private CommunicationInfo communicationInfo;
    private String addressCategory;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String state;
    private String country;
    private String pinCode;
}

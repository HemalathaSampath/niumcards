package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "KycInfo")
public class KycInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer kycInfoId;
    private String kycRefNo;
    private String documentType;
    private String documentNo;
    private String documentExpiry;
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;
}

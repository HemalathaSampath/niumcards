package com.yap.niumm2p.model.customer;

public enum DateType {
    DOB, MARRIAGE
}

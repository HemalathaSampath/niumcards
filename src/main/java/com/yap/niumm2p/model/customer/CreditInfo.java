package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class CreditInfo {
    private String cardCategory;
    private String creditLimit;
    private String score;
    private String statementDate;
    private String dueDate;
}

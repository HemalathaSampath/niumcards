package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TermsAndCondition")
public class TermsAndCondition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer termsAndConditionId;
    private Boolean accept;
    private String name;
    private String versionId;
    //Response
    private Boolean success;
    private String message;
}

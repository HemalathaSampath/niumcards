package com.yap.niumm2p.model.customer;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@Entity
//@Table(name = "KarbonCustomer")
public class KarbonCustomer1 {
    //@Id
   // @Column(name = "entity_id")
   // private String entityId;
    @JsonProperty(required = true)
    private String entityType;
    private String businessId;
    private String businessType;
    private String kitNo;
    private String cardType;
    @JsonProperty("Title")
    private String title;
    private String firstName;
    private String middleName;
    @JsonProperty(required = true)
    private String lastName;
    private Boolean isMinor;
    private Boolean isNRICustomer ;
    private Boolean isDependant ;
    private Boolean addOnCard;
    private Boolean parentLimit;
    private String maritalStatus;
    private String parentEntityId;
    private Boolean parentStatement ;
    @Embedded
    private CreditInfo creditInfo;
    @OneToMany(mappedBy = "karbonCustomer",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<KitInfo> kitInfo;
    @OneToMany(mappedBy = "karbonCustomer",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<KycInfo> kycInfo;
    @OneToMany(mappedBy = "karbonCustomer",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<DateInfo> dateInfo;
    @OneToMany(mappedBy = "karbonCustomer",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<SecurityQuestionInfo> securityQuestionInfo;
    @JsonProperty(value = "Gender")
    private String gender;
    private String language;
    @JsonProperty(value = "specialDate", required = true)
    private String dateOfBirth;
    private String contactNo;
    private String emailAddress;
    @JsonProperty(value = "countryofIssue")
    private String countryOfIssue;
    private String programType;
    private String countryCode;
    private String kycStatus;
    private String description;
    @OneToMany(mappedBy = "karbonCustomer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NomineeInfo> nomineeInfo;
    @OneToMany(mappedBy = "karbonCustomer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CommunicationInfo> communicationInfo;
    @Enumerated(EnumType.STRING)
    private EmploymentType employmentType;
    @Enumerated(EnumType.STRING)
    private EmploymentIndustry employmentIndustry;
    private String complianceStatus;
    private String customerHashId;
    private String walletHashId;
    //Sending the mini customer details we will get the redirect URL to update the all other customer details
    @Lob
    @Column( length = 100000 )
    private String redirectUrl;
    private Integer status;
}

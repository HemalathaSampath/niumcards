package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "KarbonDocuments")
public class KarbonDocuments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer karbonDocumentsId;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomerDocuments;
    private String documentType;
    private String documentNo;
    private String docExpDate;
}


package com.yap.niumm2p.model.customer;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class IdentDoc {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer identDocId;
	private String identificationType;
	private String identificationValue;
	private String identificationDocExpiry;
	private String identificationDocIssuanceCountry;
	private String identificationDocHolderName;
	private String identificationDocColor;
	private String identificationDocReferenceNumber;
	private String identificationIssuingDate;
	private String identificationIssuingAuthority;
	@OneToMany(mappedBy="identDoc")
	private List<IdentDocument> identificationDocument;
	private String status;


}

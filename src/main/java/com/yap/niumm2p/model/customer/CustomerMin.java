package com.yap.niumm2p.model.customer;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CustomerMin")
public class CustomerMin {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer customerMinId;
	private String email;
	private String countryCode;
	private String mobile;
	private String customerHashId;
	private String walletHashId;
	@Lob
	@Column( length = 100000 )
	private String redirectUrl;

}

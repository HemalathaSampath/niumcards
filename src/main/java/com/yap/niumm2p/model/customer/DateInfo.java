package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "DateInfo")
public class DateInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer dateInfoId;
    @Enumerated(EnumType.STRING)
    private DateType dateType;
    private Date date;
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;
}

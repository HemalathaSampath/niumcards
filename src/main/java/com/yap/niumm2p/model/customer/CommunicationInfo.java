package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "CommunicationInfo")
public class CommunicationInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer communicationInfoId;
    private String contactNo;
    private String contactNo2;
    private String emailId;
    private String emailId2;
    private String notification;
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;
    @OneToMany(mappedBy = "communicationInfo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AddressInfo> addressInfo;
}

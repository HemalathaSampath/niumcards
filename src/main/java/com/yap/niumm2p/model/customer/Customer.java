package com.yap.niumm2p.model.customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
@Audited
public class Customer {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer customerId;
		private String entityId;
		private String employeeId;
		private String firstName;
		private String middleName;
		private String lastName;
		private String preferredName;	
		private String dateOfBirth;	
		private String nationality;
		private String email;
		private String countryCode;
		private String mobile;	
		private String deliveryAddress1;	
		private String deliveryAddress2;
		private String deliveryCity;
		private String deliveryLandmark;	
		private String deliveryState;	
		private String deliveryZipCode;	
		private String billingAddress1;
		private String billingAddress2;	
		private String billingCity;	
		private String billingLandmark;	
		private String billingState;	
		private String billingZipCode;	
		private String correspondenceAddress1;	
		private String correspondenceAddress2;	
		private String correspondenceLandmark;	
		private String correspondenceCity;	
		private String correspondenceState;		
		private String correspondenceZipCode;
		private String complianceStatus;
		private String customerHashId;
		private String walletHashId;
}

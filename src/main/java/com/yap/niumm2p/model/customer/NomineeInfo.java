package com.yap.niumm2p.model.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "NomineeInfo")
public class NomineeInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer nomineeInfoId;
    private String nFirstName;
    private String nLastName;
    private String nMiddleName;
    private Boolean nIsMinor;
    private Integer nPercentage;
    private String nSpecialDate;
    private String nIdType;
    private String nIdNumber;
    private String nIdExpiry;
    private String nCountryofIssue;
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private KarbonCustomer karbonCustomer;

}

package com.yap.niumm2p.model.customer;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name ="RFIResponseRequest")
@Entity
@Audited
public class RFIResponseRequest{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Integer rfiResponseRequestId;
	private String rfiHashId;
	@Embedded
	private RFIIdentDoc identificationDoc;
	private String complianceId;
	private String status;
	private String rfiId;
}

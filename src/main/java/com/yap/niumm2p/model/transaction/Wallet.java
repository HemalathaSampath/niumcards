package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Wallet")
public class Wallet {
    @Id
    private Integer pkey;
    private DateTime changed;
    private String changer;
    private DateTime created;
    private String creator;
    private Integer deleted;
    private Integer version;
    private Double balance;
    @Column(name = "is_negative")
    private Integer isNegative;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "business_entity_fkey")
    private BusinessEntity businessEntity;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_fkey")
    private Product product;
}

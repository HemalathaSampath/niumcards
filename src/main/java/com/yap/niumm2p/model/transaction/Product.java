package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Product")
public class Product {
    @Id
    private Integer pkey;
    private DateTime changed;
    private String changer;
    private DateTime created;
    private String creator;
    private Integer deleted;
    private Integer version;
    private String product_name;
    private String product_id;
    private String yse_id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "corporate_fkey")
    private BusinessEntity businessEntity;
    private String crncy_cde;

}

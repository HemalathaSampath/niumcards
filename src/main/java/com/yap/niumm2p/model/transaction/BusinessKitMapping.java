package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "BusinessKitMapping")
public class BusinessKitMapping {
    @Id
    private Integer pkey;
    private DateTime changed;
    private String changer;
    private DateTime created;
    private String creator;
    private String deleted;
    private Integer version;
    private String businessId;
    @ManyToOne
    @JoinColumn(name = "merchant_fkey")
    private BusinessEntity businessEntity;
}

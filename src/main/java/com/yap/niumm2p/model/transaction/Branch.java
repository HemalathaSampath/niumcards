package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Branch")
public class Branch {
    @Id
    private Integer pkey;
    private DateTime changed;
    private String changer;
    private DateTime created;
    private String creator;
    private Integer deleted;
    private Integer version;
    private String branch_code;
    private String branch_email;
    private String branch_mobile;
    private String branch_name;
    private String branch_phone;
    private String ifsc_code;
}

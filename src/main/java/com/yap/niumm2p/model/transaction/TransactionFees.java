package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Audited
public class TransactionFees {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer transactionFeesId;
    @ManyToOne(cascade = CascadeType.ALL)
    private Authorization authorization;
    private String name;
    private String value;
    /* This is three-character ISO currency code. */
    private String currencyCode;
}

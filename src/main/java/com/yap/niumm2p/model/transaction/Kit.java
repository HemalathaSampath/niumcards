package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Kit")
@Entity
public class Kit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer pkey;
    private Date changed;
    private String changer;
    private Date   created;
    private String creator;
    private Integer deleted;
    private Integer version;
    private String card_no;
    private String kit_no;
    private String securityCode;
    private Integer status;
    private Integer account_provider_fkey;
    private Integer bus_kit_fkey;
    private Integer merchant_fkey;
    private Integer holder_fkey;
    private Date expiry_date;
    @Lob
    private Blob enc_card_no;
    @Lob
    private Blob enc_expiry_date;
    @Lob
    private Blob enc_security_code;
    private Integer card_type;
    private Integer network_type;
    private String pin_offset;
    private String pinBlock;
    private String pin_offset_pos;
    private Integer emvCardType_pkey;
    private Integer branch_fkey;
    private Integer emboss_status;
    private String product;
    private String serial_no;
    private String description;
    private Integer wallet_fkey;
    private Integer security_code_date;
}

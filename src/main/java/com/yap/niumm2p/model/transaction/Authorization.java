package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Audited
public class Authorization {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer authorizationId;
    //Contract field:
    private UUID transactionId;
    //Authorization field:
    private String transactionType;
    private UUID cardHashId;
    private String processingCode;
    private String billingAmount;
    private String transactionAmount;
    private String billingCurrencyCode;
    private String transactionCurrencyCode;
    private String dateOfTransaction;
    private String localTime;
    private String localDate;
    private String merchantCategoryCode;
    private String merchantTerminalId;
    private String merchantTerminalIdCode;
    private String merchantNameLocation;
    private String posEntryMode;
    private String posConditionCode;
    private String posEntryCapabilityCode;
    private String retrievalReferenceNumber;
    private String systemTraceAuditNumber;
    private String acquiringInstitutionCountryCode;
    private String acquiringInstitutionCode;
    private String paymentServiceFields;
    private String originalDateOfTransaction;
    private String originalSystemTraceAuditNumber;
    private String originalTransactionId;
    @OneToMany
    List<TransactionFees> transactionFeesList;
    //Reversal Field
    private String billingReplacementAmount;
    private String transactionReplacementAmount;

}

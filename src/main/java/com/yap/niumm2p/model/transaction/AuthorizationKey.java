package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "AuthorizationKey")
public class AuthorizationKey {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer authorizationKeyId;
    private String authHeaderName;
    private String authHeaderValue;
}

package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name  = "BusinessCustomerField")
public class BusinessCustomField {
    @Id
    private Integer pkey;
    private String changed;
    private String changer;
    private Date created;
    private String creator;
    private Integer   deleted;
    private Integer version;
    private String fieldName;
    private String fieldValue;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "business_fkey")
    private BusinessEntity businessEntity;
}

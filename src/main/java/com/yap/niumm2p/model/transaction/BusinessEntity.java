package com.yap.niumm2p.model.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "BusinessEntity")
public class BusinessEntity {
    @Id
    private Integer pkey;
    private String changed;
    private String changer;
    private String created;
    private String creator;
    private String deleted;
    private String version;
    private String address;
    private String address2;
    private String app_guid;
    private String auth_code;
    private String city;
    private String connectionUrl;
    private String contact_no;
    private String country;
    private String countryOfIssue;
    private String description;
    private String emailAddress;
    private String entity_id;
    private String entity_type;
    private String firstName;
    private String gender;
    private String hasCustomer;
    private String idNumber;
    private String idType;
    private String isBlocked;
    private String dependent;
    private String lastName;
    private String pincode;
    private String privateKey;
    private String publicKey;
    private String specialDate;
    private String state;
    private String status;
    private String title;
    private String yapcode;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "kit")
    private List<BusinessKitMapping> businessKitMapping;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_fkey")
    private List<Product> product;
    private String enc_yapcode;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "branch")
    private List<Branch> branches;
    private String sorEntityId;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<BusinessCustomField> businessCustomFields;
}

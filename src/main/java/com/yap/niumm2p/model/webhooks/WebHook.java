package com.yap.niumm2p.model.webhooks;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
@Audited
public class WebHook {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer webHookId;
    private String name;
    private String brandName;
    private String cardNumber;
    private String cardHashId;
    private String customerHashId;
    private String walletHashId;
    private String template;
    private String transactionAmount;
    private String transactionCurrency;
    private String merchantName;
    private String transactionDate;
    private String balanceCurrency;
    private String walletBalance;
}

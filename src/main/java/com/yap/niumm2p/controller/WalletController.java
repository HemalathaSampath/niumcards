package com.yap.niumm2p.controller;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.wallet.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.model.wallet.GetWalletBalance;
import com.yap.niumm2p.model.wallet.GetWalletTransactions;
import com.yap.niumm2p.model.wallet.P2PTransferResponse;
import com.yap.niumm2p.model.wallet.ReFundResponse;
import com.yap.niumm2p.service.WalletServices;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class WalletController {

    @Autowired
    WalletServices walletServices;

    @Autowired
    ObjectMapper mapper;

    @GetMapping("api/v1/exchangeRate")
    public ResponseEntity<Object> getExchangeRate(@RequestParam MultiValueMap<String,String> allParamsObject ) throws IOException {
        log.debug("getExchangeRate");
        ResponseEntity<GetExchangeRatePojo> result;
        try {
            result = walletServices.getExchangeRate(allParamsObject);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @GetMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}")
    public ResponseEntity<Object> getWalletBalance(@PathVariable UUID customerHashId, @PathVariable UUID walletHashId)
            throws IOException {
        log.debug("activateCard");
        ResponseEntity<GetWalletBalance[]> result ;
        try {
            result = walletServices.getWalletBalance(customerHashId, walletHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @GetMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/transactions")
    public ResponseEntity<Object> getWalletTransaction(@PathVariable UUID customerHashId,
                                                       @PathVariable UUID walletHashId, @RequestParam Map<String, String> allParams) throws IOException {
        log.debug("getWallettransaction");
        ResponseEntity<GetWalletTransactions> result;
        try {
            result = walletServices.getWalletTransaction(customerHashId, walletHashId, allParams);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/transfer")
    public ResponseEntity<Object> postBalanceTransfer(@RequestBody BalanceTransferPojo balanceTransfer,
                                                      @PathVariable UUID customerHashId, @PathVariable UUID walletHashId) throws IOException {
        log.debug("postBalanceTransfer");
        ResponseEntity<BalanceTransferResponsePojo> result;
        try {
            result = walletServices.postBalanceTransfer(balanceTransfer, customerHashId, walletHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/fund")
    public ResponseEntity<Object> postFundWallet(@RequestBody FundWalletPojo fundWallet,
                                                 @PathVariable UUID customerHashId, @PathVariable UUID walletHashId) throws IOException {
        log.debug("postFundWallet");
        ResponseEntity<FundWalletResponsePojo> result;
        try {
            result = walletServices.postFundWallet(fundWallet, customerHashId, walletHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/p2pTransfer")
    public ResponseEntity<Object> postP2PTransfer(@RequestBody P2PTransferPojo p2pTransfer,
                                                  @PathVariable UUID customerHashId, @PathVariable UUID walletHashId) throws IOException {
        log.debug("postP2PTransfer");
        ResponseEntity<P2PTransferResponse> result;
        try {
            result = walletServices.postP2PTransfer(p2pTransfer, customerHashId, walletHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/refund")
    public ResponseEntity<Object> postWithdrawfromWallet(@RequestBody ReFundPojo refund,
                                                         @PathVariable UUID customerHashId, @PathVariable UUID walletHashId) throws IOException {
        log.debug("postWithdrawfromWallet");
        ResponseEntity<ReFundResponse> result;
        try {
            result = walletServices.postWithdrawFromWallet(refund, customerHashId, walletHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

}

package com.yap.niumm2p.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.yap.niumm2p.model.customer.KarbonCustomer;
import com.yap.niumm2p.pojo.customer.*;
import com.yap.niumm2p.util.NiumConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.model.customer.CompilenceStatus;
import com.yap.niumm2p.repository.customer.CompilenceStatusRepository;
import com.yap.niumm2p.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CustomerController {

    @Autowired
    public CustomerService customerService;

    @Autowired
    public CompilenceStatusRepository compilenceStatusRepository;

    @Autowired
    ObjectMapper mapper;

    @GetMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}")
    public ResponseEntity<Object> getCustomer(@PathVariable String customerHashId) throws IOException {
        log.debug("Customer Controller :: getCustomer");
        ResponseEntity<GetCustomerPojo> result = null;
        try {
            result = customerService.getCustomer(UUID.fromString(customerHashId));
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @GetMapping("/api/v1/client/karbonCustomer/{customerHashId}/{entityId}")
    public ResponseEntity<KarbonCustomer> getKarbonCustomer(@PathVariable(required = false) String customerHashId, @PathVariable(required = false) String entityId) {


        return null;
    }

    @GetMapping("/api/v1/client/{clientHashId}/customers")
    public ResponseEntity<Object> getCustomers(@RequestParam MultiValueMap<String, String> allParams) throws IOException {
        ResponseEntity<FetchCustomerPojo[]> result = null;
        List<FetchCustomerPojo> customers = null;
        try {
            result = customerService.getCustomers(allParams);
            customers = Arrays.asList(result.getBody());
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()), HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(customers, result.getStatusCode());
    }

    @PostMapping("/api/v1/client/{clientHashId}/customer")
    public ResponseEntity<Object> postCustomer(@RequestBody KarbonCustomerPojo newCustomer) throws IOException {
        ResponseEntity<CustomerResponsePojo> result = null;
        log.debug(" Customer Controller :: PostCustomer");
        try {
            result = customerService.postCustomer(newCustomer);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("/api/v1/client/{clientHashId}/customer-min-data")
    public ResponseEntity<Object> postCustomerMinData(@RequestBody CustomerMinPojo newCustomerMin) throws IOException {
        ResponseEntity<CustomerMinResponsePojo> result = null;
        log.debug(" Customer Controller :: PostCustomerMIN");
        try {
            result = customerService.postCustomerMin(newCustomerMin);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/uploadDocuments")
    public ResponseEntity<Object> customerUploadDoc(@RequestBody(required = false) CustomerUploadDocPojo identDocumentPojo,
                                                    @PathVariable UUID customerHashId) throws IOException {
        log.debug("Controller :: customerUploadDoc");
        ResponseEntity<Object> result = null;
        try {
            result = customerService.postCustomerUploadDoc(identDocumentPojo, customerHashId);

        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping(path = "/api/v1/client/{clientHashId}/customer/{customerHashId}/uploadRfiDocument", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> customerUploadRFIDoc(@RequestBody(required = false) RFIDocumentUploadPojo uploadDoc,
                                                       @PathVariable UUID customerHashId) throws IOException {
        log.debug("Customer Controller :: customerUploadRFIDoc");
        ResponseEntity<RFIDocumentUploadResponsePojo> result = null;
        try {
            result = customerService.postCustomerUploadRFIDoc(uploadDoc, customerHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("/callback/compliance")
    public ResponseEntity<Object> receiveComplienceStatus(@RequestParam(name = "customerHashId") String customerHashId) {
        CompilenceStatus comstatus = new CompilenceStatus();
        comstatus.setCustomerHashId(UUID.fromString(customerHashId));
        compilenceStatusRepository.save(comstatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/callback/redirect?customerHashId={customerHashId}")
    public ResponseEntity<Object> getCallBack(@RequestParam UUID customerHashId) throws IOException {
        log.debug("Customer Controller :: getCallBack");
        ResponseEntity<GetCustomerPojo> result = null;
        try {
            result = customerService.getCustomer(customerHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage().split(NiumConstant.COMMA)[NiumConstant.ZERO], HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/updateCustomer")
    public ResponseEntity<Object> updateCustomer(@RequestBody UpdateCustomerPojo updateCustomer,
                                                 @PathVariable UUID customerHashId) throws IOException {
        ResponseEntity<String> result = null;
        log.debug("Customer Controller :: PostCustomer");
        try {
            result = customerService.updateCustomer(updateCustomer, customerHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage().split(NiumConstant.COMMA)[NiumConstant.ZERO], HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/termsAndConditions")
    public ResponseEntity<Object> acceptTermsAndCondition(@RequestBody TermsAndConditionPojo termsAndConditionPojo,
                                                 @PathVariable UUID customerHashId) throws IOException {
        ResponseEntity<TermsAndConditionResponsePojo> result = null;
        try {
            result = customerService.acceptTermsAndCondition(termsAndConditionPojo, customerHashId);
        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
                    HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage().split(NiumConstant.COMMA)[NiumConstant.ZERO], HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }

    @GetMapping("/api/v1/client/{clientHashId}/customers/{customerHashId}/termsAndConditions")
    public ResponseEntity<Object> getTermsAndCondition(@PathVariable UUID customerHashId) throws IOException {
        ResponseEntity<GetTermsAndConditionResponsePojo> result = null;
        try {
            result = customerService.getTermsAndCondition(customerHashId);

        } catch (RestClientResponseException e) {
            return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()), HttpStatus.valueOf(e.getRawStatusCode()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result.getBody(), result.getStatusCode());
    }
}

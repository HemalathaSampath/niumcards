package com.yap.niumm2p.controller;

import com.yap.niumm2p.pojo.webhooks.WebHookPojo;
import com.yap.niumm2p.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebHookController {

    @Autowired
    CommonService commonService;

    @PutMapping("/webhook")
    public ResponseEntity<Object> webHook(@RequestBody WebHookPojo webHookPojo) {

        commonService.webHook(webHookPojo);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

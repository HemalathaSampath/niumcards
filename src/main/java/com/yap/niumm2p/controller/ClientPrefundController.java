package com.yap.niumm2p.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.clientmanagement.ClientPrefundRequestPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.model.clientmanagement.ClientPrefundRequestResponse;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundBalance;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundRequest;
import com.yap.niumm2p.service.ClientManagementServices;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@RestController
@Slf4j
public class ClientPrefundController {

	@Autowired
	ClientManagementServices clientManagementServices;

	@Autowired
	ObjectMapper mapper;

	@PostMapping("api/v1/client/{clientHashId}/prefund")
	public ResponseEntity<Object> postClientRefundRequest(@RequestBody ClientPrefundRequestPojo clientPrefundRequest) throws IOException {
		log.debug("postClientRefundRequest");
		ResponseEntity<ClientPrefundRequestResponse> result;
		try {
			result = clientManagementServices.postClientPrefundRequest(clientPrefundRequest);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(result.getBody(), result.getStatusCode());
	}

	@GetMapping("api/v1/client/{clientHashId}/balances")
	public ResponseEntity<Object> fetchClientPrefundBalance() throws IOException {
		log.debug("fetchClientPrefundBalance");
		ResponseEntity<FetchPrefundBalance> result;
		try {
			result = clientManagementServices.fetchClientPrefundBalance();
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(result.getBody(), result.getStatusCode());
	}

	@GetMapping("api/v1/client/{clientHashId}/prefund")
	public ResponseEntity<Object> fetchClientPrefundRequest(@RequestParam MultiValueMap<String, String> allParams) throws IOException {
		log.debug("fetchClientPrefundRequest");
		ResponseEntity<FetchPrefundRequest> result;
		try {
			result = clientManagementServices.fetchClientPrefundRequest(allParams);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(result.getBody(), result.getStatusCode());
	}
}

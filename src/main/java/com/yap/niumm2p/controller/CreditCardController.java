package com.yap.niumm2p.controller;

import java.io.IOException;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.card.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.model.card.ActivateCard;
import com.yap.niumm2p.model.card.CardBlockResponse;
import com.yap.niumm2p.model.card.CardResponse;
import com.yap.niumm2p.model.card.ChannelRestrictionResponse;
import com.yap.niumm2p.model.card.CvvResponse;
import com.yap.niumm2p.model.card.GetCardResponse;
import com.yap.niumm2p.model.card.GetChannelRestrictionResponse;
import com.yap.niumm2p.model.card.PinBlockResponse;
import com.yap.niumm2p.model.card.UnmaskedcardnumberResponse;
import com.yap.niumm2p.service.CardService;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CreditCardController {

	@Autowired
	CardService cardService;
	@Autowired
	ObjectMapper mapper;

	@PostMapping("api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/activate")
	public ResponseEntity<Object> activateCard(@PathVariable UUID customerHashId, @PathVariable UUID walletHashId,
			@PathVariable UUID cardHashId) throws IOException {
		log.debug("activateCard");
		ResponseEntity<ActivateCard> result;
		try {
			result = cardService.activateCard(customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(result.getBody(), result.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card")
	public ResponseEntity<Object> addCard(@RequestBody CardPojo card, @PathVariable UUID customerHashId,
										  @PathVariable UUID walletHashId) throws IOException {
		ResponseEntity<CardResponsePojo> response;
		try {
			response = cardService.addCard(card, customerHashId, walletHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/assignCard")
	public ResponseEntity<Object> assignCard(@RequestBody AssignCardPojo assignCard, @PathVariable UUID customerHashId,
											 @PathVariable UUID walletHashId) throws IOException {
		ResponseEntity<CardResponse> response;
		try {
			response = cardService.assignCard(assignCard, customerHashId, walletHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/cardAction")
	public ResponseEntity<Object> blockORUnblockCard(@RequestBody CardBlockPojo cardblock,
			@PathVariable UUID customerHashId, @PathVariable UUID walletHashId, @PathVariable UUID cardHashId)
			throws IOException {
		log.debug("addCard");
		ResponseEntity<CardBlockResponse> response;
		try {
			response = cardService.blockORUnblockCard(cardblock, customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/channels")
	public ResponseEntity<Object> channelRestriction(@RequestBody ChannelRestrictionPojo channelRestriction,
			@PathVariable UUID customerHashId, @PathVariable UUID walletHashId, @PathVariable UUID cardHashId)
			throws IOException {
		log.debug("addCard");
		ResponseEntity<ChannelRestrictionResponse> response;
		try {
			response = cardService.channelRestriction(channelRestriction, customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@GetMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/cvv")
	public ResponseEntity<Object> fetchCVV2ForVirtualCard(@PathVariable UUID customerHashId,
			@PathVariable UUID walletHashId, @PathVariable UUID cardHashId) throws IOException {
		log.debug("fetchcvv2ForVirtualCard");
		ResponseEntity<CvvResponse> response;
		try {
			response = cardService.fetchCVV2ForVirtualCard(customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@GetMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/cards")
	public ResponseEntity<Object> getCard(@PathVariable UUID customerHashId, @PathVariable UUID walletHashId,
										  @RequestParam MultiValueMap<String, String> allParams) throws IOException {
		log.debug("getCard");
		ResponseEntity<GetCardResponse> response;
		try {
			response = cardService.getCard(customerHashId, walletHashId, allParams);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@GetMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/channels")
	public ResponseEntity<Object> getChannelRestriction(@PathVariable UUID customerHashId,
			@PathVariable UUID walletHashId, @PathVariable UUID cardHashId) throws IOException {
		log.debug("getChannelRestriction");
		ResponseEntity<GetChannelRestrictionResponse> response;
		try {
			response = cardService.getChannelRestriction(customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@GetMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/unmask")
	public ResponseEntity<Object> getUnmaskVirtualCard(@PathVariable UUID customerHashId,
			@PathVariable UUID walletHashId, @PathVariable UUID cardHashId) throws IOException {
		log.debug("getUnmaskVirtualCard");
		ResponseEntity<UnmaskedcardnumberResponse> response;
		try {
			response = cardService.getUnmaskVirtualCard(customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/replaceCard")
	public ResponseEntity<Object> issueReplacementCard(@RequestBody IssueReplacementCardPojo issueReplacementCard, @PathVariable UUID customerHashId,
													   @PathVariable UUID walletHashId, @PathVariable UUID cardHashId) throws IOException {
		log.debug("issueReplacementCard");
		ResponseEntity<CardResponse> response;
		try {
			response = cardService.issueReplacementCard(issueReplacementCard, customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

	@PostMapping("/api/v1/client/{clientHashId}/customer/{customerHashId}/wallet/{walletHashId}/card/{cardHashId}/pin")
	public ResponseEntity<Object> pinUpdate(@RequestBody PinBlockPojo pinBlock, @PathVariable UUID customerHashId,
			@PathVariable UUID walletHashId, @PathVariable UUID cardHashId) throws IOException {
		log.debug("pinUpdate");
		ResponseEntity<PinBlockResponse> response;
		try {
			response = cardService.pinUpdate(pinBlock, customerHashId, walletHashId, cardHashId);
		} catch (RestClientResponseException e) {
			return new ResponseEntity<>(mapper.readTree(e.getResponseBodyAsString()),
					HttpStatus.valueOf(e.getRawStatusCode()));
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response.getBody(), response.getStatusCode());
	}

}

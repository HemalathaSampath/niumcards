package com.yap.niumm2p.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.exception.TransactionNotAuthorizedException;
import com.yap.niumm2p.pojo.auth.AuthorizationPojo;
import com.yap.niumm2p.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {

    @Autowired
    CommonService commonService;

    @PostMapping("/api/v1/authorization")
    public ResponseEntity<Object> getAuthorization(@RequestBody(required = true) AuthorizationPojo authorization) throws JsonProcessingException, TransactionNotAuthorizedException {
        ResponseEntity<Object> response;
        try {
            response = commonService.authorization(authorization);
        }catch (TransactionNotAuthorizedException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return response;
    }
}

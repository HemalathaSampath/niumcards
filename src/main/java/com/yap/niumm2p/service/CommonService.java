package com.yap.niumm2p.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.exception.TransactionNotAuthorizedException;
import com.yap.niumm2p.pojo.auth.AuthorizationPojo;
import com.yap.niumm2p.pojo.webhooks.WebHookPojo;
import com.yap.niumm2p.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CommonService {

    @Autowired
    CommonUtil commonUtil;

    public void webHook(WebHookPojo webHook){
        commonUtil.webHook(webHook);
    }

    public ResponseEntity<Object> authorization(AuthorizationPojo authorization) throws TransactionNotAuthorizedException, JsonProcessingException {

        return commonUtil.authorization(authorization);
    }
}

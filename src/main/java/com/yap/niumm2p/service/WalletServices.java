package com.yap.niumm2p.service;

import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.wallet.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.yap.niumm2p.model.wallet.GetWalletBalance;
import com.yap.niumm2p.model.wallet.GetWalletTransactions;
import com.yap.niumm2p.model.wallet.P2PTransferResponse;
import com.yap.niumm2p.model.wallet.ReFundResponse;
import com.yap.niumm2p.util.WalletRequestResponseUtil;
import org.springframework.util.MultiValueMap;

@Service
public class WalletServices {
	@Autowired
	WalletRequestResponseUtil walletRequestResponseUtil;

	public ResponseEntity<GetExchangeRatePojo> getExchangeRate(MultiValueMap<String,String> allParams)   {

		return walletRequestResponseUtil.getExchangeRate(allParams);
	}
	public ResponseEntity<GetWalletBalance[]> getWalletBalance(UUID customerHashId, UUID walletHashId)   {

		return walletRequestResponseUtil.getWalletBalance(customerHashId, walletHashId);
	}
	public ResponseEntity<GetWalletTransactions> getWalletTransaction(UUID customerHashId, UUID walletHashId, Map<String, String> allParams)   {

		return walletRequestResponseUtil.getWalletTransaction(customerHashId, walletHashId, allParams);
	}
	public ResponseEntity<BalanceTransferResponsePojo> postBalanceTransfer(BalanceTransferPojo balanceTransfer, UUID customerHashId, UUID walletHashId
			) throws JsonProcessingException {

		return walletRequestResponseUtil.postBalanceTransfer(balanceTransfer, customerHashId, walletHashId);
	}
	
	public ResponseEntity<FundWalletResponsePojo> postFundWallet(FundWalletPojo fundWallet, UUID customerHashId, UUID walletHashId) {

		return walletRequestResponseUtil.postFundWallet(fundWallet, customerHashId, walletHashId);
	}
	public ResponseEntity<P2PTransferResponse> postP2PTransfer(P2PTransferPojo p2pTransfer, UUID customerHashId, UUID walletHashId
			) throws JsonProcessingException {

		return walletRequestResponseUtil.postP2PTransfer(p2pTransfer, customerHashId, walletHashId);
	}
	public ResponseEntity<ReFundResponse> postWithdrawFromWallet(ReFundPojo refund, UUID customerHashId, UUID walletHashId
			) throws JsonProcessingException {

		return walletRequestResponseUtil.postWithdrawFromWallet(refund, customerHashId, walletHashId);
	}
	
}

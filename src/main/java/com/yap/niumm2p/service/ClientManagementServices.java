package com.yap.niumm2p.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.clientmanagement.ClientPrefundRequestPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.yap.niumm2p.model.clientmanagement.ClientPrefundRequestResponse;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundBalance;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundRequest;
import com.yap.niumm2p.util.ClientManagementRequestResponseUtil;
import org.springframework.util.MultiValueMap;


@Service
public class ClientManagementServices {
	
	@Autowired
	public ClientManagementRequestResponseUtil clientManagementRequestResponseutil;
	
	public ResponseEntity<ClientPrefundRequestResponse> postClientPrefundRequest(ClientPrefundRequestPojo clientPrefundRequest) throws JsonProcessingException {
		
		return clientManagementRequestResponseutil.postClientPrefundRequest(clientPrefundRequest);
	}
	
	public ResponseEntity<FetchPrefundRequest> fetchClientPrefundRequest(MultiValueMap<String, String> allParams) {
		return clientManagementRequestResponseutil.fetchClientPrefundRequest(allParams);
	}
	
	public ResponseEntity<FetchPrefundBalance> fetchClientPrefundBalance() {
		return clientManagementRequestResponseutil.fetchClientPrefundBalance();
	} 
	
}

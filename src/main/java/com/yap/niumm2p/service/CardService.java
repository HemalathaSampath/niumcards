package com.yap.niumm2p.service;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.card.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.yap.niumm2p.model.card.ActivateCard;
import com.yap.niumm2p.model.card.CardBlockResponse;
import com.yap.niumm2p.model.card.CardResponse;
import com.yap.niumm2p.model.card.ChannelRestrictionResponse;
import com.yap.niumm2p.model.card.CvvResponse;
import com.yap.niumm2p.model.card.GetCardResponse;
import com.yap.niumm2p.model.card.GetChannelRestrictionResponse;
import com.yap.niumm2p.model.card.PinBlockResponse;
import com.yap.niumm2p.model.card.UnmaskedcardnumberResponse;
import com.yap.niumm2p.util.CardRequestResponseUtil;
import org.springframework.util.MultiValueMap;

@Service
public class CardService {

    @Autowired
    CardRequestResponseUtil cardRequestResponseUtil;

    public ResponseEntity<ActivateCard> activateCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        return cardRequestResponseUtil.activateCard(customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<CardResponsePojo> addCard(CardPojo card, UUID customerHashId, UUID walletHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.addCard(card, customerHashId, walletHashId);
    }

    public ResponseEntity<CardResponse> assignCard(AssignCardPojo assignCardPojo, UUID customerHashId, UUID walletHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.assignCard(assignCardPojo, customerHashId, walletHashId);
    }

    public ResponseEntity<CardBlockResponse> blockORUnblockCard(CardBlockPojo cardblock, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.blockORUnblockCard(cardblock, customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<ChannelRestrictionResponse> channelRestriction(ChannelRestrictionPojo channelRestriction, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.channelRestriction(channelRestriction, customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<CvvResponse> fetchCVV2ForVirtualCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        return cardRequestResponseUtil.fetchCVV2ForVirtualCard(customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<GetCardResponse> getCard(UUID customerHashId, UUID walletHashId, MultiValueMap<String, String> allParams) {

        return cardRequestResponseUtil.getCard(customerHashId, walletHashId, allParams);
    }

    public ResponseEntity<GetChannelRestrictionResponse> getChannelRestriction(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        return cardRequestResponseUtil.getChannelRestriction(customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<UnmaskedcardnumberResponse> getUnmaskVirtualCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        return cardRequestResponseUtil.getUnmaskVirtualCard(customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<CardResponse> issueReplacementCard(IssueReplacementCardPojo issueReplacementCard, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.issueReplacementCard(issueReplacementCard, customerHashId, walletHashId, cardHashId);
    }

    public ResponseEntity<PinBlockResponse> pinUpdate(PinBlockPojo pinBlock, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        return cardRequestResponseUtil.pinUpdate(pinBlock, customerHashId, walletHashId, cardHashId);
    }

}

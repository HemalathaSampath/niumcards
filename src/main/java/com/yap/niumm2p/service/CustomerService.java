package com.yap.niumm2p.service;

import java.io.IOException;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.customer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.yap.niumm2p.util.CustomerRequestResponseUtil;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CustomerService {

    @Autowired
    private CustomerRequestResponseUtil customerRequestResponseUtil;

    public ResponseEntity<CustomerResponsePojo> postCustomer(KarbonCustomerPojo customer) throws IOException {

        return customerRequestResponseUtil.postCustomer(customer);
    }

    public ResponseEntity<CustomerMinResponsePojo> postCustomerMin(CustomerMinPojo customerMin) {

        return customerRequestResponseUtil.postMinCustomer(customerMin);
    }

    public ResponseEntity<String> updateCustomer(UpdateCustomerPojo updateCustomer, UUID customerHashId) throws JsonProcessingException {

        return customerRequestResponseUtil.updateCustomer(updateCustomer, customerHashId);
    }

    public ResponseEntity<GetCustomerPojo> getCustomer(UUID customerHashId) {

        return customerRequestResponseUtil.getCustomer(customerHashId);
    }

    public ResponseEntity<FetchCustomerPojo[]> getCustomers(MultiValueMap<String, String> allParams) {

        return customerRequestResponseUtil.fetchCustomer(allParams);
    }

    public ResponseEntity<Object> postCustomerUploadDoc(CustomerUploadDocPojo identDocumentPojo, UUID customerHashId) {

        return customerRequestResponseUtil.postCustomerUploadDoc(identDocumentPojo, customerHashId);
    }

    public ResponseEntity<RFIDocumentUploadResponsePojo> postCustomerUploadRFIDoc(RFIDocumentUploadPojo rfiDocumentUploadPojo, UUID customerHashId) throws JsonProcessingException {

        return customerRequestResponseUtil.postCustomerUploadRFIDoc(rfiDocumentUploadPojo, customerHashId);
    }

    public ResponseEntity<TermsAndConditionResponsePojo> acceptTermsAndCondition( TermsAndConditionPojo termsAndConditionPojo, UUID customerHashId) throws JsonProcessingException {

        return customerRequestResponseUtil.acceptTermsAndCondition(termsAndConditionPojo, customerHashId);
    }

    public ResponseEntity<GetTermsAndConditionResponsePojo> getTermsAndCondition(UUID customerHashId) throws JsonProcessingException {

        return customerRequestResponseUtil.getTermsAndCondition(customerHashId);
    }

}

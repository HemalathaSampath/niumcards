package com.yap.niumm2p.util;

import java.util.Map;
import java.util.UUID;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.wallet.*;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.yap.niumm2p.model.wallet.GetWalletBalance;
import com.yap.niumm2p.model.wallet.GetWalletTransactions;
import com.yap.niumm2p.model.wallet.P2PTransferResponse;
import com.yap.niumm2p.model.wallet.ReFundResponse;
import com.yap.niumm2p.repository.wallet.WalletRepositoryUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class WalletRequestResponseUtil {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private WalletRepositoryUtil walletRepositoryUtil;

	@Autowired
	ObjectMapper mapper;

	public ResponseEntity<BalanceTransferResponsePojo> postBalanceTransfer(BalanceTransferPojo balanceTransfer,
																		   UUID customerHashId, UUID walletHashId) throws JsonProcessingException {

		ResponseEntity<BalanceTransferResponsePojo> result;
		String jsonString = mapper.writeValueAsString(balanceTransfer);
		result = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId
						+ NiumConstant.TRANSFER,
				HttpMethod.POST, URLGeneration.generateEntity(jsonString), BalanceTransferResponsePojo.class);
		walletRepositoryUtil.saveBalanceTransfer(balanceTransfer, customerHashId, walletHashId, result);
		return result;
	}

	public ResponseEntity<P2PTransferResponse> postP2PTransfer(P2PTransferPojo p2pTransfer, UUID customerHashId,
			UUID walletHashId) throws JsonProcessingException {

		ResponseEntity<P2PTransferResponse> result;
		String jsonString = mapper.writeValueAsString(p2pTransfer);
		result = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId
						+ NiumConstant.P2P_TRANSFER,
				HttpMethod.POST, URLGeneration.generateEntity(jsonString), P2PTransferResponse.class);
		walletRepositoryUtil.saveP2PTransfer(p2pTransfer, customerHashId, walletHashId, result);
		return result;
	}

	@SneakyThrows
	public ResponseEntity<FundWalletResponsePojo> postFundWallet(FundWalletPojo fundWallet, UUID customerHashId,
																 UUID walletHashId) {
		ResponseEntity<FundWalletResponsePojo> result;
		String jsonString = mapper.writeValueAsString(fundWallet);
		result = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId
						+ NiumConstant.FUND,
				HttpMethod.POST, URLGeneration.generateEntity(jsonString), FundWalletResponsePojo.class);
		walletRepositoryUtil.saveFundWallet(fundWallet, customerHashId, walletHashId, result);
		return result;
	}

	public ResponseEntity<ReFundResponse> postWithdrawFromWallet(ReFundPojo refund, UUID customerHashId, UUID walletHashId)
			throws JsonProcessingException {
		ResponseEntity<ReFundResponse> result;
		String jsonString = mapper.writeValueAsString(refund);
		result = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId
						+ NiumConstant.REFUND,
				HttpMethod.POST, URLGeneration.generateEntity(jsonString), ReFundResponse.class);
		walletRepositoryUtil.saveWithdrawFromWallet(refund, customerHashId, walletHashId, result);
		return result;
	}

	public ResponseEntity<GetWalletTransactions> getWalletTransaction(UUID customerHashId, UUID walletHashId,
			Map<String, String> allParams)  {

		ResponseEntity<GetWalletTransactions> response;
		response = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId
						+ NiumConstant.TRANSACTIONS,
				HttpMethod.GET, URLGeneration.generateEntity(String.class), GetWalletTransactions.class,
				allParams);
		walletRepositoryUtil.saveWalletTransaction(customerHashId, walletHashId, response);
		return response;
	}

	public ResponseEntity<GetWalletBalance[]> getWalletBalance(UUID customerHashId, UUID walletHashId){
		ResponseEntity<GetWalletBalance[]> result;
		result = restTemplate.exchange(
				NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId, HttpMethod.GET,
				URLGeneration.generateEntity(String.class), GetWalletBalance[].class);
		return result;
	}

	public ResponseEntity<GetExchangeRatePojo> getExchangeRate(MultiValueMap<String,String> allParams) {
	
		ResponseEntity<GetExchangeRatePojo> response;
		HttpEntity<Object> entity = URLGeneration.generateEntity(String.class);
		log.debug("entity" + entity.toString());
		UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
				.scheme("https").host("apisandbox.spend.nium.com")
				.path("/api/v1/exchangeRate")
				.queryParams(allParams);
		response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET,
				entity, GetExchangeRatePojo.class);
		return response;
	}

}


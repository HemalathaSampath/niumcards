package com.yap.niumm2p.util;

public class NiumConstant {
    private NiumConstant() {
    }

    public static final String X_API_KEY = "x-api-key";
    public static final String X_REQUEST_ID = "x-request-id";
    public static final String X_CLIENT_NAME = "x-client-name";
    public static final String X_API_KEY_VALUE = "Os5ytlpn5R2NJSCmEHCAqaWBFWNluXFG8DE1hDmL";
    public static final String X_CLIENT_NAME_VALUE = "YAP";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String SCHEME = "https";
    public static final String HOST_NAME = "apisandbox.spend.nium.com";
    public static final String CLIENT_ID = "1828b4e3-1663-403f-a784-e91cbf007b64";
    public static final String NIUM_BASE_URL = "https://apisandbox.spend.nium.com/api/v1/client/";
    public static final String PATH = "/api/v1/client/" + CLIENT_ID;
    public static final String NIUM_URL_CUSTOMER = NIUM_BASE_URL + CLIENT_ID + "/customer/";
    public static final String NIUM_URL_PREFUND = PATH  + "/prefund";
    public static final String NIUM_URL_BALANCES = PATH + "/balances";
    public static final String NIUM_URL_CUSTOMERS = PATH + CLIENT_ID +"/customers";
    public static final String CUSTOMER_MIN_DATA = NIUM_BASE_URL + CLIENT_ID + "/customer-min-data/";
    public static final String TERMS_AND_CONDITIONS = "/termsAndConditions";
    public static final String WALLET = "/wallet/";
    public static final String CARD = "/card/";
    public static final String CARDS = "/cards/";
    public static final String ACTIVATE = "/activate/";
    public static final String PIN = "/pin/";
    public static final String REPLACE_CARD = "/replaceCard/";
    public static final String CARD_ACTION = "/cardAction/";
    public static final String UNMASK = "/unmask/";
    public static final String CVV = "/cvv/";
    public static final String ASSIGN_CARD = "/assignCard/";
    public static final String CHANNELS = "/channels/";
    public static final String TRANSFER = "/transfer/";
    public static final String P2P_TRANSFER = "/p2pTransfer/";
    public static final String FUND = "/fund/";
    public static final String REFUND = "/refund/";
    public static final String TRANSACTIONS = "/transactions/";
    public static final String UPDATE_CUSTOMER = "/updateCustomer/";
    public static final String EXCHANGE_RATE = "https://apisandbox.spend.nium.com/api/v1/exchangeRate";
    public static final String UPLOAD_DOCUMENT = "/uploadDocuments";
    public static final String UPLOAD_RFI_DOCUMENT = "/uploadRfiDocument";
    public static final Integer ZERO = 0;
    public static final Integer ONE = 1;
    public static final String COMMA = ",";

}

package com.yap.niumm2p.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.clientmanagement.ClientPrefundRequestPojo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.yap.niumm2p.model.clientmanagement.ClientPrefundRequest;
import com.yap.niumm2p.model.clientmanagement.ClientPrefundRequestResponse;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundBalance;
import com.yap.niumm2p.model.clientmanagement.FetchPrefundRequest;
import com.yap.niumm2p.repository.clientmanagement.ClientPrefundRequestRepository;
import com.yap.niumm2p.repository.clientmanagement.FetchPrefundBalanceRepository;
import com.yap.niumm2p.repository.clientmanagement.FetchPrefundRequestRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class ClientManagementRequestResponseUtil {

    private RestTemplate restTemplate;
    private ClientPrefundRequestRepository clientPrefundRequestRepository;
    private FetchPrefundBalanceRepository fetchPrefundBalanceRepository;
    private FetchPrefundRequestRepository fetchPrefundRequestRepository;
    private ObjectMapper mapper;

    @Autowired
    public ClientManagementRequestResponseUtil(RestTemplate restTemplate, ClientPrefundRequestRepository clientPrefundRequestRepository, FetchPrefundBalanceRepository fetchPrefundBalanceRepository, FetchPrefundRequestRepository fetchPrefundRequestRepository, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.clientPrefundRequestRepository = clientPrefundRequestRepository;
        this.fetchPrefundBalanceRepository = fetchPrefundBalanceRepository;
        this.fetchPrefundRequestRepository = fetchPrefundRequestRepository;
        this.mapper = mapper;
    }

    public ResponseEntity<ClientPrefundRequestResponse> postClientPrefundRequest(ClientPrefundRequestPojo clientPrefundRequest) throws JsonProcessingException {
        log.debug("ClientPrefundRequest" + clientPrefundRequest.toString());
        ResponseEntity<ClientPrefundRequestResponse> result;
        String jsonString = mapper.writeValueAsString(clientPrefundRequest);

        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme(NiumConstant.SCHEME).host(NiumConstant.HOST_NAME)
                .path(NiumConstant.NIUM_URL_PREFUND);
        result = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST,
                URLGeneration.generateEntity(jsonString), ClientPrefundRequestResponse.class);
        ClientPrefundRequest clientPrefundRequest1 = new ClientPrefundRequest();
        BeanUtils.copyProperties(clientPrefundRequest, clientPrefundRequest1);
        clientPrefundRequest1.setResponse(result.getBody());
        clientPrefundRequestRepository.save(clientPrefundRequest1);
        return result;
    }

    public ResponseEntity<FetchPrefundRequest> fetchClientPrefundRequest(MultiValueMap<String, String> allParams) {
        log.debug("fetchClientPrefundRequest Parameter" + allParams);
        ResponseEntity<FetchPrefundRequest> response;
        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme(NiumConstant.SCHEME).host(NiumConstant.HOST_NAME)
                .path(NiumConstant.NIUM_URL_PREFUND)
                .queryParams(allParams);
        response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET,
                URLGeneration.generateEntity(String.class), FetchPrefundRequest.class);
        fetchPrefundRequestRepository.save(response.getBody());
        return response;
    }

    public ResponseEntity<FetchPrefundBalance> fetchClientPrefundBalance() {
        ResponseEntity<FetchPrefundBalance> response;
        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme(NiumConstant.SCHEME).host(NiumConstant.HOST_NAME)
                .path(NiumConstant.NIUM_URL_BALANCES);
        response = restTemplate.exchange(uriComponents.toUriString(),
                HttpMethod.GET,
                URLGeneration.generateEntity(String.class),
                FetchPrefundBalance.class);
        fetchPrefundBalanceRepository.save(response.getBody());
        return response;
    }
}

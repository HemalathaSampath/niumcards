package com.yap.niumm2p.util;

public class KarbonConstant {

    private KarbonConstant() {
    }
    public static final String HOST_NAME = "uat.yorecard.com";
    public static final String SCHEME = "https";
    public static final String PATH = "/api/transactions/api";
}

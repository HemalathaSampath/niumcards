package com.yap.niumm2p.util.karbontoniumconvertor;

import com.yap.niumm2p.model.customer.KarbonAddress;
import com.yap.niumm2p.pojo.customer.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.websocket.Encoder;
import java.util.*;

@Slf4j
@Component
public class CustomerPojoConvert {

private static List<Country> countries = new ArrayList<>();
private static String[] countryCodes = Locale.getISOCountries();

    @PostConstruct
    public static void init() {
        for (String countryCode : countryCodes){
            Locale locale = new Locale("", countryCode);
            String iso = locale.getISO3Country();
            String code = locale.getCountry();
            String name = locale.getDisplayCountry();
            countries.add(new Country(iso, code, name));
        }
    }

   public CustomerPojo karbonToCustomerCovert(KarbonCustomerPojo karbonCustomerPojo){

       CustomerPojo customerPojo = new CustomerPojo();
       BeanUtils.copyProperties(karbonCustomerPojo, customerPojo);
       customerPojo.setDateOfBirth(karbonCustomerPojo.getDateOfBirth().toString());
       customerPojo.setMobile(karbonCustomerPojo.getContactNo().substring(3,13));
       customerPojo.setEmail(karbonCustomerPojo.getEmailAddress());
       customerPojo.setPreferredName(karbonCustomerPojo.getFirstName());
       customerPojo.setBillingAddress1(karbonCustomerPojo.getAddress());
       customerPojo.setBillingAddress2(karbonCustomerPojo.getAddress2());
       customerPojo.setBillingCity(karbonCustomerPojo.getCity());
       customerPojo.setBillingState(karbonCustomerPojo.getState());
       customerPojo.setBillingZipCode(karbonCustomerPojo.getPinCode());
       customerPojo.setNationality(karbonCustomerPojo.getCountry());
       customerPojo.setCountryCode( getCountryCode(karbonCustomerPojo.getCountry().toUpperCase()));
       Optional< List<KarbonAddress>> karbonAddressList = Optional.ofNullable(karbonCustomerPojo.getAddressDto().getAddress());
       if (karbonAddressList.isPresent()) {
           for(KarbonAddress karbonAddress: karbonAddressList.get()) {
               if(karbonAddress.getTitle().equalsIgnoreCase(AddressCategory.HOME.toString()) ||
               karbonAddress.getTitle().equalsIgnoreCase(AddressCategory.OFFICE.toString())){
                   customerPojo.setCorrespondenceAddress1(karbonAddress.getAddress1());
                   customerPojo.setCorrespondenceAddress2(karbonAddress.getAddress2() + karbonAddress.getAddress3());
                   customerPojo.setCorrespondenceCity(karbonAddress.getCity());
                   customerPojo.setCorrespondenceState(karbonAddress.getState());
                   customerPojo.setCorrespondenceZipCode(karbonAddress.getPinCode());
               }else if(karbonAddress.getTitle().equalsIgnoreCase(AddressCategory.DELIVERY.toString()) ||
                       karbonAddress.getTitle().equalsIgnoreCase(AddressCategory.COMMUNICATION.toString()) ||
                       karbonAddress.getTitle().equalsIgnoreCase(AddressCategory.PERMANENT.toString())){
                   customerPojo.setDeliveryAddress1(karbonAddress.getAddress1());
                   customerPojo.setDeliveryAddress2(karbonAddress.getAddress2() +karbonAddress.getAddress3());
                   customerPojo.setDeliveryCity(karbonAddress.getCity());
                   customerPojo.setDeliveryState(karbonAddress.getState());
                   customerPojo.setDeliveryZipCode(karbonAddress.getPinCode());
               }
           }
       }
       return customerPojo;
    }

    private String getCountryCode(String country) {
        String countryCode = null;

        for (Country countryObj : countries){
            if(country.equalsIgnoreCase(countryObj.getName())){
                countryCode = countryObj.getCode();
            }
        }
      return countryCode;
    }

    public void convertuploadDocument(CustomerUploadDocPojo customerUploadDocPojo)  {
        // pass the image to be converted
        Optional<List<IdentDocPojo>> identDocPojos = Optional.ofNullable(customerUploadDocPojo.getIdentificationDoc());
        if(identDocPojos.isPresent()){
            for(IdentDocPojo identDocPojo: identDocPojos.get()){
                Optional<List<IdentDocumentPojo>>  identDocumentPojos= Optional.ofNullable(identDocPojo.getIdentificationDocument());
                if(identDocumentPojos.isPresent()){
                    for(IdentDocumentPojo  identDocumentPojo: identDocumentPojos.get()){
                       String binaryString =  covertBase64(identDocumentPojo.getDocument());
                     //  identDocumentPojo.setDocument(covertBase64(identDocumentPojo.getDocument()));
                    }
                }
            }
        }
        //System.out.println( covertBase64(multipartFile.toString()));
    }

    private String covertBase64(String multipartFile) {
        StringBuilder sb = new StringBuilder();
       // sb.append("data:image/png;base64,");
        sb.append(Base64.getEncoder().encodeToString(multipartFile.getBytes()));
        log.debug("Coverted image ::" + sb.toString());
        return sb.toString();
    }
    public String convertStringToBinary(String input) {

        StringBuilder result = new StringBuilder();
        char[] chars = input.toCharArray();
        for (char aChar : chars) {
            result.append(
                    String.format("%8s", Integer.toBinaryString(aChar))   // char -> int, auto-cast
                            .replaceAll(" ", "0")                         // zero pads
            );
        }
        return result.toString();

    }
}

package com.yap.niumm2p.util.karbontoniumconvertor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Country {
    private String iso;
    private String code;
    private String name;
}

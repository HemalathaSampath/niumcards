package com.yap.niumm2p.util;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.card.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.yap.niumm2p.model.card.ActivateCard;
import com.yap.niumm2p.model.card.CardBlockResponse;
import com.yap.niumm2p.model.card.CardResponse;
import com.yap.niumm2p.model.card.ChannelRestrictionResponse;
import com.yap.niumm2p.model.card.CvvResponse;
import com.yap.niumm2p.model.card.GetCardResponse;
import com.yap.niumm2p.model.card.GetChannelRestrictionResponse;
import com.yap.niumm2p.model.card.PinBlockResponse;
import com.yap.niumm2p.model.card.UnmaskedcardnumberResponse;
import com.yap.niumm2p.repository.card.CardRepositoryUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CardRequestResponseUtil {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    CardRepositoryUtil cardRepositoryUtil;

    public ResponseEntity<CardResponsePojo> addCard(CardPojo card, UUID customerHashId, UUID walletHashId) throws JsonProcessingException {

        ResponseEntity<CardResponsePojo> response;
        String jsonString = mapper.writeValueAsString(card);
        response = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                CardResponsePojo.class);
        CardResponse cardResponse = new CardResponse();
        BeanUtils.copyProperties(response.getBody(), cardResponse);
        cardRepositoryUtil.saveCard(card, cardResponse);
        return response;
    }

    public ResponseEntity<ActivateCard> activateCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        ResponseEntity<ActivateCard> result;
        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.ACTIVATE,
                HttpMethod.POST,
                URLGeneration.generateEntity(String.class),
                ActivateCard.class);
        cardRepositoryUtil.saveActivateCard(customerHashId, walletHashId, cardHashId, result);
        return result;
    }

    public ResponseEntity<PinBlockResponse> pinUpdate(PinBlockPojo pinBlock, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        ResponseEntity<PinBlockResponse> result;
        String jsonString = mapper.writeValueAsString(pinBlock);
        log.debug("Pinupdate" + pinBlock);

        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.PIN,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                PinBlockResponse.class);

        log.debug("Pinupdate Status" + result.getBody());
        cardRepositoryUtil.savePinUpdate(pinBlock, customerHashId, walletHashId, cardHashId, result);
        return result;
    }

    public ResponseEntity<CardResponse> issueReplacementCard(IssueReplacementCardPojo issueReplacementCard, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        ResponseEntity<CardResponse> result;
        String jsonString = mapper.writeValueAsString(issueReplacementCard);
        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.REPLACE_CARD,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                CardResponse.class);
        cardRepositoryUtil.saveIssueReplacementCard(issueReplacementCard, customerHashId, walletHashId, result);
        return result;
    }

    public ResponseEntity<CardBlockResponse> blockORUnblockCard(CardBlockPojo cardblock, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {

        ResponseEntity<CardBlockResponse> result;
        String jsonString = mapper.writeValueAsString(cardblock);

        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.CARD_ACTION,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                CardBlockResponse.class);
        cardRepositoryUtil.saveBlockORUnblockCard(cardblock, customerHashId, walletHashId, cardHashId, result);
        return result;
    }

    public ResponseEntity<CardResponse> assignCard(AssignCardPojo assignCard, UUID customerHashId, UUID walletHashId) throws JsonProcessingException {

        ResponseEntity<CardResponse> result;
        String jsonString = mapper.writeValueAsString(assignCard);
        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.ASSIGN_CARD,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                CardResponse.class);
        cardRepositoryUtil.saveAssignCard(assignCard, customerHashId, walletHashId, result);

        return result;
    }

    public ResponseEntity<ChannelRestrictionResponse> channelRestriction(ChannelRestrictionPojo channelRestriction, UUID customerHashId, UUID walletHashId, UUID cardHashId) throws JsonProcessingException {
        ResponseEntity<ChannelRestrictionResponse> result;
        String jsonString = mapper.writeValueAsString(channelRestriction);
        result = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.CHANNELS,
                HttpMethod.POST,
                URLGeneration.generateEntity(jsonString),
                ChannelRestrictionResponse.class);
        cardRepositoryUtil.saveChannelRestriction(channelRestriction, customerHashId, walletHashId, cardHashId, result);
        return result;
    }

    public ResponseEntity<GetCardResponse> getCard(UUID customerHashId, UUID walletHashId, MultiValueMap<String, String> allParams) {
        ResponseEntity<GetCardResponse> response;
        response = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARDS,
                HttpMethod.GET,
                URLGeneration.generateEntity(String.class),
                GetCardResponse.class,
                allParams);
        return response;
    }

    public ResponseEntity<UnmaskedcardnumberResponse> getUnmaskVirtualCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {
        ResponseEntity<UnmaskedcardnumberResponse> response;
        response = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.UNMASK,
                HttpMethod.GET,
                URLGeneration.generateEntity(String.class),
                UnmaskedcardnumberResponse.class);

        return response;
    }

    public ResponseEntity<CvvResponse> fetchCVV2ForVirtualCard(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        ResponseEntity<CvvResponse> response;

        response = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.CVV,
                HttpMethod.GET,
                URLGeneration.generateEntity(String.class),
                CvvResponse.class);

        return response;
    }

    public ResponseEntity<GetChannelRestrictionResponse> getChannelRestriction(UUID customerHashId, UUID walletHashId, UUID cardHashId) {

        ResponseEntity<GetChannelRestrictionResponse> response;

        response = restTemplate.exchange(
                NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.WALLET + walletHashId + NiumConstant.CARD + cardHashId + NiumConstant.CHANNELS,
                HttpMethod.GET,
                URLGeneration.generateEntity(String.class),
                GetChannelRestrictionResponse.class);
        return response;
    }
}

package com.yap.niumm2p.util;

import java.io.IOException;
import java.util.UUID;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yap.niumm2p.pojo.customer.*;
import com.yap.niumm2p.repository.customer.CustomerRepositoryUtil;
import com.yap.niumm2p.util.karbontoniumconvertor.CustomerPojoConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Component
public class CustomerRequestResponseUtil {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    private CustomerRepositoryUtil customerRepositoryUtil;

    @Autowired
    private CustomerPojoConvert customerPojoConvert;

    HttpHeaders headers = null;

    public ResponseEntity<CustomerResponsePojo> postCustomer(KarbonCustomerPojo karbonCustomerPojo) throws IOException {
        ResponseEntity<CustomerResponsePojo> result;
       CustomerPojo customerPojo =  customerPojoConvert.karbonToCustomerCovert(karbonCustomerPojo);
        result = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER, HttpMethod.POST,
                URLGeneration.generateEntity(customerPojo), CustomerResponsePojo.class);
        log.debug("After: customer Rest API call" + result.getBody().toString());
        karbonCustomerPojo.setStatus(NiumConstant.ZERO);
        customerRepositoryUtil.saveCustomer(customerPojo, karbonCustomerPojo, result);
        return result;
    }

    public ResponseEntity<CustomerMinResponsePojo> postMinCustomer(CustomerMinPojo customerMinPojo)   {
        ResponseEntity<CustomerMinResponsePojo> result;
        result = restTemplate.exchange(NiumConstant.CUSTOMER_MIN_DATA, HttpMethod.POST,
                URLGeneration.generateEntity(customerMinPojo), CustomerMinResponsePojo.class);
        log.debug("After: customerMinDataResponse Rest API call" + result.getBody().toString());
        customerRepositoryUtil.saveMinCustomer(customerMinPojo, result);
        return result;
    }


    public ResponseEntity<String> updateCustomer(UpdateCustomerPojo updateCustomerPojo, UUID customerHashId) throws JsonProcessingException {
        ResponseEntity<String> result;
        String jsonString = mapper.writeValueAsString(updateCustomerPojo);
        result = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.UPDATE_CUSTOMER,
                HttpMethod.POST, URLGeneration.generateEntity(jsonString), String.class);
        customerRepositoryUtil.saveUpdateCustomer(updateCustomerPojo, customerHashId.toString());
        return result;
    }

    public ResponseEntity<GetCustomerPojo> getCustomer(UUID customerHashId) {
        ResponseEntity<GetCustomerPojo> response;
        log.debug("Get Customer");
        response = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER + customerHashId, HttpMethod.GET,
                URLGeneration.generateEntity(String.class), GetCustomerPojo.class);
        log.debug("Get Customer Response" + response.getBody());
        customerRepositoryUtil.saveGetCustomer(response, customerHashId.toString());
        return response;
    }

    public ResponseEntity<Object> postCustomerUploadDoc(CustomerUploadDocPojo customerUploadDocPojo, UUID customerHashId)  {
        ResponseEntity<Object> result;
        customerPojoConvert.convertuploadDocument(customerUploadDocPojo);
        result = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.UPLOAD_DOCUMENT,
                HttpMethod.POST, URLGeneration.generateEntity(customerUploadDocPojo), Object.class);
        log.debug("UploadDoc Response: " + result.getBody());
        customerRepositoryUtil.saveCustomerUploadDoc(customerUploadDocPojo, result);
        return result;
    }

    public ResponseEntity<FetchCustomerPojo[]> fetchCustomer(MultiValueMap<String, String> allParams) {
        ResponseEntity<FetchCustomerPojo[]> response;
        log.debug("map param" + allParams);
        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme(NiumConstant.SCHEME).host(NiumConstant.HOST_NAME)
                .path(NiumConstant.NIUM_URL_CUSTOMERS)
                .queryParams(allParams);
        response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET,
                URLGeneration.generateEntity(String.class), FetchCustomerPojo[].class);
        return response;
    }

    public ResponseEntity<RFIDocumentUploadResponsePojo> postCustomerUploadRFIDoc(RFIDocumentUploadPojo rfiDocumentUploadPojo, UUID customerHashId) throws JsonProcessingException {
        ResponseEntity<RFIDocumentUploadResponsePojo> result;
        String jsonString = mapper.writeValueAsString(rfiDocumentUploadPojo);
        result = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.UPLOAD_RFI_DOCUMENT,
                HttpMethod.POST, URLGeneration.generateEntity(jsonString), RFIDocumentUploadResponsePojo.class);
        customerRepositoryUtil.saveRFIUploadDocument(rfiDocumentUploadPojo, result);
        return result;
    }


    public ResponseEntity<GetTermsAndConditionResponsePojo> getTermsAndCondition( UUID customerHashId) {
        ResponseEntity<GetTermsAndConditionResponsePojo> response;
        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme(NiumConstant.SCHEME).host(NiumConstant.HOST_NAME)
                .path(NiumConstant.NIUM_URL_CUSTOMERS + customerHashId+NiumConstant.TERMS_AND_CONDITIONS);
        response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.GET,
                URLGeneration.generateEntity(String.class), GetTermsAndConditionResponsePojo.class);
        return response;
    }

    public ResponseEntity<TermsAndConditionResponsePojo> acceptTermsAndCondition(TermsAndConditionPojo termsAndConditionPojo, UUID customerHashId) throws JsonProcessingException {
        ResponseEntity<TermsAndConditionResponsePojo> result;
        result = restTemplate.exchange(NiumConstant.NIUM_URL_CUSTOMER + customerHashId + NiumConstant.TERMS_AND_CONDITIONS,
                HttpMethod.POST, URLGeneration.generateEntity(mapper.writeValueAsString(termsAndConditionPojo)), TermsAndConditionResponsePojo.class);
        customerRepositoryUtil.saveTermsAndCondition(termsAndConditionPojo, result);
        return result;
    }

}

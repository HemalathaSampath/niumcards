package com.yap.niumm2p.util;

import java.util.UUID;
import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class URLGeneration {
	
	private static HttpHeaders headers = null;
			private URLGeneration(){}
	
	@PostConstruct
	public static void init() {
		headers = new HttpHeaders();
		UUID xRequestId = UUID.randomUUID();
		headers.set(NiumConstant.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(NiumConstant.X_API_KEY, NiumConstant.X_API_KEY_VALUE);
		headers.set(NiumConstant.X_REQUEST_ID, xRequestId.toString());
		headers.set(NiumConstant.X_CLIENT_NAME, NiumConstant.X_CLIENT_NAME_VALUE);
		headers.set("cache-control", "no-cache");
	}	
	
	public static HttpEntity<Object> generateEntity(Object object) {
		log.debug("Object ::" + object.toString());
		log.debug("headers ::" + headers.toString());
		return new HttpEntity<>(object, headers);
	}

}

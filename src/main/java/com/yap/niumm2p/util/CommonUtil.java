package com.yap.niumm2p.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yap.niumm2p.exception.TransactionNotAuthorizedException;
import com.yap.niumm2p.model.transaction.Kit;
import com.yap.niumm2p.model.webhooks.WebHook;
import com.yap.niumm2p.pojo.auth.AuthorizationPojo;
import com.yap.niumm2p.pojo.webhooks.WebHookPojo;
import com.yap.niumm2p.repository.common.KitRepository;
import com.yap.niumm2p.repository.common.WebHookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.Optional;

@Slf4j
@Component
@PropertySource("classpath:application.properties")
public class CommonUtil {

    private WebHookRepository webHookRepository;
    private KitRepository kitRepository;
    private RestTemplate restTemplate;
    private ObjectMapper mapper;
    @Value("${niumcard.authorizationEnabled}")
    private String authorizationEnabled;

    @Autowired
    public CommonUtil(WebHookRepository webHookRepository, KitRepository kitRepository, RestTemplate restTemplate, ObjectMapper mapper) {
        this.webHookRepository = webHookRepository;
        this.kitRepository = kitRepository;
        this.restTemplate = restTemplate;
        this.mapper = mapper;
    }

    public void webHook(WebHookPojo webHookPojo){
        WebHook webHook = new WebHook();
        BeanUtils.copyProperties(webHook, webHookPojo);
        webHookRepository.save(webHook);
    }

    public ResponseEntity<Object> authorization(AuthorizationPojo authorization) throws TransactionNotAuthorizedException, JsonProcessingException {
        ResponseEntity<Object> response;
       Optional<Kit> kit =  kitRepository.findbycard_no(Integer.parseInt(authorization.getCardHashId().toString()));
       if(kit.isPresent()){
            log.debug("Kit is Present. Kit : " + kit.toString());
            String jsonString = mapper.writeValueAsString(authorization);
           UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                   .scheme(KarbonConstant.SCHEME).host(KarbonConstant.HOST_NAME)
                   .path(KarbonConstant.PATH);
           response = restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST,
                   URLGeneration.generateEntity(jsonString), Object.class);
        }else {
           throw new TransactionNotAuthorizedException();
       }
        return response;
    }
}

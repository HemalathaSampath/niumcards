package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageablePojo {
				
		private String sort;
		private String pageSize;
		private String pageNumber;
		private String offset;
		private Boolean paged;
		private Boolean unpaged;
		private Boolean last;
		private String totalElements;
		private String totalPages;
		private String numberOfElements;
		private Boolean first;
		private String size;
		private String number;
		private Boolean empty;
		

}

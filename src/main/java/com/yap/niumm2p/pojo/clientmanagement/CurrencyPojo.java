package com.yap.niumm2p.pojo.clientmanagement;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyPojo {

		private String id;
		private String createdAt;
		private String updatedAt;
		private String isoCode;
		private String curSymbol;
		private String localCode;
		private String unit;
		private String clientId;
		private String authorizationOrder;
		@JsonProperty("default")
		private String defaultt;
}

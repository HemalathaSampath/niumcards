package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientAccountsPojo {
		

		private String id;
		private String createdAt;
		private String updatedAt;
		private String accountType;
		private String balance;
			
}

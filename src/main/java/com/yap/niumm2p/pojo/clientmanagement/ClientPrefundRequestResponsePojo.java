package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientPrefundRequestResponsePojo {
					
		private String message;
		private String status;
		private Double amount;
		private String systemReferenceNumber;

}

package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchPrefundRequestPojo {

		private List<PrefundContentPojo> content;
		private PageablePojo pageable;

}

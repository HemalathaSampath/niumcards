package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientPrefundRequestPojo {
		private String bankReferenceNumber;
		private String clientAccountNumber;
		private String comments;
		private String currencyCode;
		private String dateOfTransfer;
		private String niumAccountNumber;
		private String amount;
		private String requesterId;
}

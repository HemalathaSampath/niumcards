package com.yap.niumm2p.pojo.clientmanagement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrefundContentPojo {

		private String id;
		private String createdAt;
		private String updatedAt;
		private Double amount;
		private String bankReferenceNumber;
		private String imageUrl;
		private String comments;
		private String status;
		private String approverId;
		private String requesterId;
		private String systemReferenceNumber;
		private String dateOfTransfer;
		private String currencyCode;
		private String clientAccountNumber;
		private String niumAccountNumber;
		private String clientId;
		private ClientAccountsPojo clientAccounts;
		private CurrencyPojo currency;

}

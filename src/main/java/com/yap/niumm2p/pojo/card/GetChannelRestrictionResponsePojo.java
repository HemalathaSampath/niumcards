package com.yap.niumm2p.pojo.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetChannelRestrictionResponsePojo {
	private String status;
	private String message;
	private String code;
	private BodyPojo body;
}

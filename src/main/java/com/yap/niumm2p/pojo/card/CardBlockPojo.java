package com.yap.niumm2p.pojo.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CardBlockPojo {
	@JsonProperty(required = true)
	private String reason;
	@JsonProperty(required = true)
	private String blockAction;
}

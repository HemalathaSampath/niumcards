package com.yap.niumm2p.pojo.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChannelRestrictionPojo {
	
	private String action;
	private String channel;

}

package com.yap.niumm2p.pojo.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignCardPojo {
	@JsonProperty(required = true)
	private String accountNumber;
	@JsonProperty(required = true)
	private String cardNumberLast4Digits;

}

package com.yap.niumm2p.pojo.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardContentPojo {

	private String cardHashId;
	private String maskedCardNumber;
	private String cardType;
	private String cardStatus;
	private String proxyNumber;
	private String embossingLine1;
	private String embossingLine2;
	private String logoId;
	private String plasticId;
	private String blockReason;
	private String replacedOn;
	private String countryCode;
	private String issuanceMode;
	private String issuanceType;
	private Boolean isNonPerso;
	private String createdAt;
	private String updatedAt;

}

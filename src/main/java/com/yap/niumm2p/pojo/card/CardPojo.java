package com.yap.niumm2p.pojo.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardPojo {
		@JsonProperty(required = true)
	  private String cardIssuanceAction;
	@JsonProperty(required = true)
	  private String cardFeeCurrencyCode;
	  private int cardExpiry;
	@JsonProperty(required = true)
	  private String cardType;
	@JsonProperty(required = true)
	  private String embossingLine1;
	  private String embossingLine2;
	@JsonProperty(required = true)
	  private boolean nonPerso;
	  private String issuanceMode;
	@JsonProperty(required = true)
	  private int logoId;
	@JsonProperty(required = true)
	  private int plasticId;
}

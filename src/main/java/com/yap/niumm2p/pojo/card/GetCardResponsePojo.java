package com.yap.niumm2p.pojo.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCardResponsePojo {

	private String totalPages;
	private List<CardContentPojo> content;
	private String totalElements;
	
}

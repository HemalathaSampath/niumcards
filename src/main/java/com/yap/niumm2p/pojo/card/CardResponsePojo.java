package com.yap.niumm2p.pojo.card;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardResponsePojo {

	private UUID cardHashId;
	private String cardActivationStatus;
	private String maskedCardNumber;

}

package com.yap.niumm2p.pojo.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentDocPojo {
	@JsonProperty(required = true)
	private String identificationType;
	private String identificationValue;
	private String identificationDocExpiry;

	private String identificationDocIssuanceCountry;
	private String identificationDocHolderName;
	private String identificationDocColor;
	private String identificationDocReferenceNumber;
	private String identificationIssuingDate;
	private String identificationIssuingAuthority;
	@JsonProperty(required = true)
	private List<IdentDocumentPojo> identificationDocument;
	@JsonIgnore
	private String status;
}

package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTermsAndConditionResponsePojo {
    private String name;
    private String versionId;
    private String description;
    private String createdAt;
}

package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FetchCustomerPojo {

	private String createdAt;
	private String updatedAt;
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String preferredName;
	private String dateOfBirth;
	private String gender;
	private String designation;
	private String employeeId;
	private String email;
	private String mobile;
	private String country;
	private String kycMode;
	private String nationality;
	private Boolean kycCustomer;
	private String complianceStatus;
	private UUID customerHashId;
	private UUID walletHashId;
}

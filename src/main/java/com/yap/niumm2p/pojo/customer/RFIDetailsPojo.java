package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RFIDetailsPojo {
    private UUID rfiHashId;
    private String description;
    private String documentType;
    private String remarks;
}

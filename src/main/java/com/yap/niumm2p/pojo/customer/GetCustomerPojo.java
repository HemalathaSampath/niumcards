package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCustomerPojo {
	private String title;
	 private String firstName;
	 private String middleName;
	 private String lastName;
	 private String preferredName;
	 private String dateOfBirth;
	 private String gender;
	 private String designation;
	 private String employeeId;
	 private String nationality;
	 private String countryCode;
	 private String deliveryAddress1;
	 private String deliveryAddress2;
	 private String deliveryCity;
	 private String deliveryLandmark;
	 private String deliveryState;
	 private String deliveryZipCode;
	 private String billingAddress1;
	 private String billingAddress2;
	 private String billingCity;
	 private String billingLandmark;
	 private String billingState;
	 private String billingZipCode;
	 private String correspondenceAddress1;
	 private String correspondenceAddress2;
	 private String correspondenceCity;
	 private String correspondenceLandmark;
	 private String correspondenceState;
	 private String correspondenceZipCode;
	 private String complianceStatus;
	 List<RFIDetailsPojo> rfiDetails;
}

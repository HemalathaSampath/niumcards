package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityQuestionInfoPojo {

    private String securityQuestion;
    private String securityAnswer;
}

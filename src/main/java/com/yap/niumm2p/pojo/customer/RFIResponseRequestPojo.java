package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RFIResponseRequestPojo {

	private String rfiHashId;
	private IdentDocPojo identificationDoc;
}

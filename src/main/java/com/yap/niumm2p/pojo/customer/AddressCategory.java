package com.yap.niumm2p.pojo.customer;

public enum AddressCategory {

    PERMANENT,
    HOME,
    OFFICE,
    DELIVERY,
    COMMUNICATION
}
package com.yap.niumm2p.pojo.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerMinPojo {
	@JsonProperty(required = true)
	private String email;
	@JsonProperty(required = true)
	private String countryCode;
	@JsonProperty(required = true)
	private String mobile;
	private String customerHashId;
}

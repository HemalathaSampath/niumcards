package com.yap.niumm2p.pojo.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TermsAndConditionResponsePojo {
    private Boolean success;
    private String message;
}

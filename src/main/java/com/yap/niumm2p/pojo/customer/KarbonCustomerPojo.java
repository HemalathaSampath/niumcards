package com.yap.niumm2p.pojo.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yap.niumm2p.model.customer.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KarbonCustomerPojo {

    private String entityId;
    @JsonProperty(required = true)
    private String entityType;
    private String businessId;
    private String businessType;
    private String kitNo;
    private String cardType;
    @JsonProperty("Title")
    private String title;
    private String firstName;
    private String middleName;
    @JsonProperty(required = true)
    private String lastName;
    @JsonProperty(value = "Gender")
    private String gender;
    @JsonProperty(value = "specialDate", required = true)
    private String dateOfBirth;
    private String contactNo;
    private String emailAddress;
  //  @JsonProperty(value = "Address")
    private String address;
    private String address2;
  //  @JsonProperty(value = "City")
    private String city;
  //  @JsonProperty(value = "State")
    private String state;
    private String country;
    @JsonProperty(value = "pincode")
    private String pinCode;
    private String idType;
    private String idNumber;
    private String idExpiry;
    @JsonProperty(value = "countryofIssue")
    private String countryOfIssue;
    private String programType;
    private String countryCode;
    private String kycStatus;
    private List<KarbonDocuments> documents;
    private String eKycRefNo;
    private String description;
    private KarbonAddressDTO addressDto;
    private String complianceStatus;
    private String customerHashId;
    private String walletHashId;
    //Sending the mini customer details we will get the redirect URL to update the all other customer details
    private String redirectUrl;
    private Integer status;
}

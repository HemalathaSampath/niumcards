package com.yap.niumm2p.pojo.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.sql.Blob;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentDocumentPojo {
	@JsonProperty(required = true)
	private String fileName;
	@JsonProperty(required = true)
	private String fileType;
	@JsonProperty(required = true)
	private String document;
}

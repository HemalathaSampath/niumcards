package com.yap.niumm2p.pojo.customer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerPojo {
    @JsonIgnore
    private String entityId;
    @JsonProperty(required = true)
    private String firstName;
    private String middleName;
    @JsonProperty(required = true)
    private String lastName;
    @JsonProperty(required = true)
    private String preferredName;
    @JsonProperty(required = true)
    private String dateOfBirth;
    private String gender;
    private String designation;
    private String employeeId;
    @JsonProperty(required = true)
    private String nationality;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private String countryCode;
    @JsonProperty(required = true)
    private String mobile;
    @JsonProperty(required = true)
    private String deliveryAddress1;
    @JsonProperty(required = true)
    private String deliveryAddress2;
    @JsonProperty(required = true)
    private String deliveryCity;
    private String deliveryLandmark;
    @JsonProperty(required = true)
    private String deliveryState;
    @JsonProperty(required = true)
    private String deliveryZipCode;
    @JsonProperty(required = true)
    private String billingAddress1;
    private String billingAddress2;
    private String billingLandmark;
    @JsonProperty(required = true)
    private String billingCity;
    @JsonProperty(required = true)
    private String billingState;
    @JsonProperty(required = true)
    private String billingZipCode;
    private String correspondenceAddress1;
    private String correspondenceAddress2;
    private String correspondenceLandmark;
    private String correspondenceCity;
    private String correspondenceState;
    private String correspondenceZipCode;
    private UUID customerHashId;
}

package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class P2PTransferPojo {

	private String destinationWalletHashId;
	private Double amount;
	private String currencyCode;

}

package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundWalletResponsePojo {

	private String systemReferenceNumber;
	private Double destinationAmount;
	private String destinationCurrencyCode;
	private Double sourceAmount;
	private String sourceCurrencyCode;
	private List<PaymentMethodsPojo> paymentMethods;

}

package com.yap.niumm2p.pojo.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetWalletBalancePojo {

	private String curSymbol;
	private Double balance;
	private Double withHoldingBalance;
	private List<MccDataPojo> mccData;
	private String pocketName;
	@JsonProperty("default")
	private Boolean defaultt;
	private String isoCode;
}

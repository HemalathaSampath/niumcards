package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetExchangeRatePojo {

	private String status;
	private String message;
	private String code;
	private GetExchangeBody body;

}

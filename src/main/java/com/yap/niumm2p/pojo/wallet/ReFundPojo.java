package com.yap.niumm2p.pojo.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReFundPojo {
	@JsonProperty(required = true)
	private Double amount;
	@JsonProperty(required = true)
	private String comments;
	@JsonProperty(required = true)
	private String currencyCode;
	private String pocketName;
	@JsonProperty(required = true)
	private String refundMode;
}

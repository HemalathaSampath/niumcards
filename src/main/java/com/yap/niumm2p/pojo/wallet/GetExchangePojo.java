package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetExchangePojo {
	
	private Double amount;
	private String destinationCurrencyCode;
	private String sourceCurrencyCode;
}

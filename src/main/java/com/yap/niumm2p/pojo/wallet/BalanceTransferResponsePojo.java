package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class BalanceTransferResponsePojo {
	
	private String fxRate;
	private String sourceCurrency;
	private String destinationCurrency;
	private Double markupAmount;
	private Double markupAmountPercentage;
	private Double sourceAmount;
	private Double destinationAmount;
}

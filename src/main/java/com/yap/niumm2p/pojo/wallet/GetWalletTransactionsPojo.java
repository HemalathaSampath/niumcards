package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetWalletTransactionsPojo {

	private String totalPages;
	private List<ContentPojo> content;
	private String totalElements;

}

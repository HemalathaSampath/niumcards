package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ContentPojo {

	private String acquirerCountryCode;
	private String acquiringInstitutionCode;
	private String authCurrencyCode;
	private UUID cardHashId;
	private Double cardTransactionAmount;
	private String createdAt;
	private String billingCurrencyCode;
	private String previousBalance;
	private String currentWithHoldingBalance;
	private String dateOfTransaction;
	private Boolean debit;
	private Double effectiveAuthAmount;
	private String maskedCardNumber;
	private String mcc;
	private String merchantID;
	private String merchantName;
	private String merchantCity;
	private String merchantCountry;
	private String posConditionCode;
	private String posEntryCapabilityCode;
	private String processingCode;
	private Double billingReplacementAmount;
	private Double transactionReplacementAmount;
	private String retrievalReferenceNumber;
	private Double settlementAmount;
	private String settlementStatus;
	private String status;
	private String systemTraceAuditNumber;
	private String terminalID;
	private String authCode;
	private String transactionCurrencyCode;
	private String transactionType;
	private String updatedAt;
	private String rhaTransactionId;
	private String partnerReferenceNumber;
	private String transactionDescription;
	
}

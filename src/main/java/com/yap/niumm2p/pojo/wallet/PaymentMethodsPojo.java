package com.yap.niumm2p.pojo.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentMethodsPojo {
	
	private String bankName;
	private String accountNumber;
	private String accountName;
	private String qrCode;
	@JsonProperty("UEN")
	private String uen;
	private String status;
	
}

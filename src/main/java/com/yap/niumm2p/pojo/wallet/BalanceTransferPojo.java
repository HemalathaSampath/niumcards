package com.yap.niumm2p.pojo.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BalanceTransferPojo {
	@JsonProperty(required = true)
	private String amount;
	@JsonProperty(required = true)
	private String destinationCurrency;
	@JsonProperty(required = true)
	private String sourceCurrency;

}

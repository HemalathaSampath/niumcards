package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetExchangeBody {

    private Double exchangeRate;
    private String lastUpdated;
    private String sourceCurrencyCode;
    private String destinationCurrencyCode;
    private Double markupRate;
    private Double markupAmount;
    private Double sourceAmount;
    private Double destinationAmount;
    private Boolean hasBalance;
}

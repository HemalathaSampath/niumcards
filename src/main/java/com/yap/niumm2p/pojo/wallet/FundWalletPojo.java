package com.yap.niumm2p.pojo.wallet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FundWalletPojo {

	private String fundingChannel;
	private Double amount;
	private String pocketName;
	private String sourceCurrencyCode;
	private String destinationCurrencyCode;

}

package com.yap.niumm2p.pojo.webhooks;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebHookPojo {

    private String name;
    private String brandName;
    private String cardNumber;
    private String cardHashId;
    private String customerHashId;
    private String walletHashId;
    private String template;
    private String transactionAmount;
    private String transactionCurrency;
    private String merchantName;
    private String transactionDate;
    private String balanceCurrency;
    private String walletBalance;

}

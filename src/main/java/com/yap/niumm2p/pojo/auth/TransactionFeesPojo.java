package com.yap.niumm2p.pojo.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionFeesPojo {

    private String name;
    private String value;
    /* This is three-character ISO currency code. */
    private String currencyCode;
}

package com.yap.niumm2p.pojo.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorizationPojo {

    //Contract field:
    private UUID transactionId;
    //Authorization field:
    private String transactionType;
    private UUID cardHashId;
    private String processingCode;
    private String billingAmount;
    private String transactionAmount;
    private String billingCurrencyCode;
    private String transactionCurrencyCode;
    private String dateOfTransaction;
    private String localTime;
    private String localDate;
    private String merchantCategoryCode;
    private String merchantTerminalId;
    private String merchantTerminalIdCode;
    private String merchantNameLocation;
    private String posEntryMode;
    private String posConditionCode;
    private String posEntryCapabilityCode;
    private String retrievalReferenceNumber;
    private String systemTraceAuditNumber;
    private String acquiringInstitutionCountryCode;
    private String acquiringInstitutionCode;
    private String paymentServiceFields;
    private String originalDateOfTransaction;
    private String originalSystemTraceAuditNumber;
    private String originalTransactionId;
    List<TransactionFeesPojo> transactionFeesList;
    //Reversal Field
    private String billingReplacementAmount;
    private String transactionReplacementAmount;

}

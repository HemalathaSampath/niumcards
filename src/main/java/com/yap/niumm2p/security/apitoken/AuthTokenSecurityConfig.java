package com.yap.niumm2p.security.apitoken;

import com.yap.niumm2p.model.transaction.AuthorizationKey;
import com.yap.niumm2p.repository.common.AuthorizationKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
@Order(1)
public class AuthTokenSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${niumcard.http.auth.tokenName}")
    private String authHeaderName;

    //TODO: retrieve this token value from data source
    @Value("${niumcard.http.auth.tokenValue}")
    private String authHeaderValue;

   // @Autowired
   // private AuthorizationKeyRepository  authorizationKeyRepository;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
     //  Optional<List<AuthorizationKey>> authorizationKeyList= Optional.ofNullable(authorizationKeyRepository.findAll());
        PreAuthTokenHeaderFilter filter = new PreAuthTokenHeaderFilter(authHeaderName);
     /*    AtomicReference<Boolean> authorization = new AtomicReference<>(false);
       if(authorizationKeyList.isPresent()) {
           for (AuthorizationKey authorizationKey : authorizationKeyList.get()) {
               filter.setAuthenticationManager((Authentication authentication) -> {
                   if (authHeaderValue.equals(authentication.getPrincipal())) {
                       authorization.set(true);
                   }
                   if(authorization){

                   }
               });
           }
       }*/


        filter.setAuthenticationManager((Authentication authentication) -> {
            if (!authHeaderValue.equals(authentication.getPrincipal()))
                throw new BadCredentialsException("The API key was not found " + "or not the expected value.");
            authentication.setAuthenticated(true);
            return authentication;
        });

        httpSecurity.
                antMatcher("/api/**")
                .csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(filter)
                .addFilterBefore(new ExceptionTranslationFilter(
                                new Http403ForbiddenEntryPoint()),
                        filter.getClass()
                )
                .authorizeRequests()
                .anyRequest()
                .authenticated();
    }

}

package com.yap.niumm2p.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TransactionNotAuthorizedException extends Exception{
    private static final  HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
    private static final  String STATUS_MESSAGE =  "Transaction Not Authorized";

}
